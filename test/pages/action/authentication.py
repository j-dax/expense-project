from config.web_config import WEB_URL
from time import sleep


def login_as_manager(driver):
    """Open a new tab and hit the login button"""
    driver.get(f'{WEB_URL}/login')

    login_form = driver.find_element_by_id("login-form")
    login_form.find_element_by_name("username").send_keys("admin")
    login_form.find_element_by_name("password").send_keys("admin")
    login_form.find_element_by_xpath("input[3]").click()


def login_as_employee(driver):
    """Open a new tab and hit the login button"""
    driver.get(f'{WEB_URL}/login')
    login_form = driver.find_element_by_id("login-form")
    login_form.find_element_by_name("username").send_keys("jameson")
    login_form.find_element_by_name("password").send_keys("gambler")
    login_form.find_element_by_xpath("input[3]").click()


def logout(driver):
    """Open a new tab and hit the logout button"""
    driver.get(f'{WEB_URL}/login')
    driver.find_element_by_id("login-div").click()
