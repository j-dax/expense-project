from time import sleep
from unittest import TestCase
from pages.action import authentication


class TestAuthentication(TestCase):
    def test_login_redirects_to_claim(self):
        driver = authentication.login_as_employee()
        self.assertEqual(driver.current_url, "http://localhost:3000/claim")
        driver.close()
        
        driver = authentication.login_as_manager()
        self.assertEqual(driver.current_url, "http://localhost:3000/claim")
        driver.close()
        