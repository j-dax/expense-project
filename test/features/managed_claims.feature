Feature: Manager changes the status of a report

Scenario: Manager views a particular report
    Given the Manager is logged in and is on a particular report page
    When the Manager fills out a description and selects a status
    Then the Manager will see an updated report.

Scenario: Manager reviews a report
    Given the Manager is logged in and is on a particular report page
    When the manager asks for more information and selects Pending
    Then the Manager will see an updated report.
