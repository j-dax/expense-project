from behave import given, when


@given(u'An employee is logged in')
def employee_logs_in(context):
    context.browser.get(f"{context.urls['FRONT_END']}/login")
    login_form = context.browser.find_element_by_id("login-form")
    login_form.find_element_by_name("username").send_keys("emp2")
    login_form.find_element_by_name("password").send_keys("gambler")
    login_form.find_element_by_xpath("input[3]").click()


@when(u'the employee submits a claim')
def submit_claim(context):
    # context.browser.get(f"{context.urls['FRONT_END']}/claim")
    context.browser.find_element_by_id("modal-button").click()

    claim_form = context.browser.find_element_by_id("claim-form")
    claim_form.find_element_by_name("description").send_keys("test submit claim form")
    claim_form.find_element_by_name("amount").send_keys("50.00")
    claim_form.find_element_by_xpath("input[3]").click()
