from behave import given, when, then


@given('the Manager is logged in and is on a particular report page')
def manager_logs_in(context):
    context.browser.get(f"{context.urls['FRONT_END']}/login")
    login_form = context.browser.find_element_by_id("login-form")
    login_form.find_element_by_name("username").send_keys("admin")
    login_form.find_element_by_name("password").send_keys("admin")
    login_form.find_element_by_xpath("input[3]").click()

    context.browser.find_element_by_id("login-div").find_element_by_tag_name("button")


@when('the Manager fills out a description and selects a status')
def status_buttons_visible(context):
    context.browser.get(f"{context.urls['FRONT_END']}/report/0")
    context.browser.find_element_by_id("response-input").send_keys("something meaningful")
    # why is xpath 1-indexed?
    btn_group = context.browser.find_element_by_class_name("btn-group")
    btn_group.find_element_by_xpath("button[1]")
    btn_group.find_element_by_xpath("button[2]")
    btn_group.find_element_by_xpath("button[3]")


@then('the Manager will see an updated report.')
def report_visible(context):
    context.browser.get(f"{context.urls['FRONT_END']}/report/0")
    context.browser.find_element_by_id("response-container")


@when('the manager asks for more information and selects Pending')
def response_set(context):
    context.browser.get(f"{context.urls['FRONT_END']}/report/0")
    context.browser.find_element_by_id("response-input").send_keys("need more information")
    context.browser.find_element_by_class_name("btn-group").find_element_by_xpath("button[3]").click()
