from selenium import webdriver
from behave import fixture, use_fixture
from time import sleep


@fixture
def browser_firefox(context):
    context.browser = webdriver.Firefox()
    yield context.browser
    context.browser.quit()


def before_all(context):
    use_fixture(browser_firefox, context)
    context.urls = {
        "FRONT_END": "http://localhost:3000",
        "API": "http://localhost:5000/api"
    }

    context.browser.implicitly_wait(5)
    context.browser.get(f"{context.urls['API']}/logout")


def after_step(context, step):
    sleep(2)
