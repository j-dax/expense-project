from src.dao.dao_helper import cursor_handler
from src.config.logging_config import route_logger as logger

logger.name = "claim_dao"


@cursor_handler
def create_claim(amount, claim_body, empl_id, claim_id=-1, cursor=None):
    if claim_id >= 0:
        cursor.execute(
            """INSERT INTO claim
                    (claim_id,claim_status,amount,claim_body,filed_by)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING claim_id,last_update,claim_status,amount,claim_body,filed_by""",
            (claim_id, "PENDING", amount, claim_body, empl_id),
        )
    else:  # let the db figure it out
        cursor.execute(
            """INSERT INTO claim
                    (claim_status, amount, claim_body, filed_by)
                    VALUES (%s, %s, %s, %s)
                    RETURNING claim_id, last_update, claim_status, amount, claim_body, filed_by""",
            ("PENDING", amount, claim_body, empl_id),
        )
    result = cursor.fetchone()
    logger.info(f"create_claim :: {result}")
    return result


@cursor_handler
def get_all_claims(cursor=None):
    cursor.execute("""SELECT * FROM claim""")
    result = cursor.fetchall()
    logger.info(f"get_all_claims :: {result}")
    return result


@cursor_handler
def get_claims_by_employee_id(employee_id, cursor=None):
    cursor.execute(
        """SELECT * FROM claim
                WHERE filed_by = %s""",
        (employee_id,),
    )
    result = cursor.fetchall()
    logger.info(f"get_claim_by_id :: {result}")
    return result


@cursor_handler
def get_claim_by_id(claim_id, cursor=None):
    cursor.execute(
        """SELECT * FROM claim
                WHERE claim_id = %s""",
        (claim_id,),
    )
    result = cursor.fetchone()
    logger.info(f"get_claim_by_id :: {result}")
    return result


@cursor_handler
def update_claim(claim_id, claim_status, amount, claim_body, employee_id, cursor=None):
    cursor.execute(
        """UPDATE claim
                SET amount=%s, filed_by=%s, claim_body=%s, claim_status=%s
                WHERE claim_id = %s
                RETURNING claim_id,last_update,claim_status,amount,claim_body,filed_by""",
        (amount, employee_id, claim_body, claim_status, claim_id),
    )
    result = cursor.fetchone()
    logger.info(f"update_claim :: {result}")
    return result


@cursor_handler
def delete_claim_by_id(claim_id, cursor=None):
    cursor.execute(
        """DELETE FROM claim
                        WHERE claim_id = %s""",
        (claim_id,),
    )
    logger.info(f"delete_claim_by_id")


@cursor_handler
def count_of_claims_by_claimant(cursor):
    cursor.execute(
        # claim_id, last_update, claim_status, amount, claim_body, filed_by
        """SELECT * FROM (
        SELECT filed_by, COUNT(claim_id) claims_count FROM claim GROUP BY filed_by
        ) AS agg_data ORDER BY agg_data.claims_count"""
    )
    result = cursor.fetchall()
    logger.info(f"most_claims :: {result}")
    return result


@cursor_handler
def sum_of_claim_amount_by_claimant(cursor):
    cursor.execute(
        # claim_id, last_update, claim_status, amount, claim_body, filed_by
        """SELECT * FROM (
        SELECT filed_by, SUM(amount) amount_sum FROM claim GROUP BY filed_by
        ) AS agg_data ORDER BY agg_data.amount_sum DESC"""
    )
    result = cursor.fetchall()
    logger.info(f"most_claims :: {result}")
    return result


@cursor_handler
def claimant_data(cursor):
    cursor.execute(
        """SELECT * FROM (
        SELECT filed_by, SUM(amount) amount_sum, COUNT(claim_id) FROM claim GROUP BY filed_by
        ) AS agg_data ORDER BY agg_data.amount_sum DESC"""
    )
    result = cursor.fetchall()
    logger.info(f"most_claims :: {result}")
    return result
