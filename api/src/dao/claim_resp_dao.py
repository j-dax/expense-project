from src.dao.dao_helper import cursor_handler
from src.config.logging_config import route_logger as logger

logger.name = "claim_resp_dao"


@cursor_handler
def create_claim_response(resp_body, claim_id, response_id=-1, cursor=None):
    if response_id >= 0:
        cursor.execute(
            """INSERT INTO claim_response
                    (response_id, resp_body, claim_id)
                    VALUES (%s, %s, %s)
                    RETURNING response_id, resp_body, last_update, claim_id""",
            (response_id, resp_body, claim_id),
        )
    else:
        cursor.execute(
            """INSERT INTO claim_response
                    (resp_body, claim_id)
                    VALUES (%s, %s)
                    RETURNING response_id, resp_body, last_update, claim_id""",
            (resp_body, claim_id),
        )
    result = cursor.fetchone()
    logger.info(f"create_claim_response :: {result}")
    return result


@cursor_handler
def get_claim_response(resp_id, cursor=None):
    cursor.execute(
        """SELECT * FROM claim_response
                   WHERE response_id = %s""",
        (resp_id,),
    )
    result = cursor.fetchone()
    logger.info(f"get_claim_response :: {result}")
    return result


@cursor_handler
def get_claim_response_by_claim_id(claim_id, cursor=None):
    cursor.execute(
        """SELECT * FROM claim_response
    WHERE claim_id = %s""",
        (claim_id,),
    )
    result = cursor.fetchone()
    logger.info(f"get_claim_response_by_claim_id :: {result}")
    return result


@cursor_handler
def update_claim_response(resp_id, resp_body, claim_id, cursor=None):
    cursor.execute(
        """UPDATE claim_response
                SET resp_body=%s, claim_id=%s
                WHERE response_id = %s
                RETURNING response_id, resp_body, last_update, claim_id""",
        (resp_body, claim_id, resp_id),
    )
    result = cursor.fetchone()
    logger.info(f"update_claim_response :: {result}")
    return result


@cursor_handler
def delete_claim_response(response_id, cursor=None):
    cursor.execute(
        """DELETE FROM claim_response
                WHERE response_id = %s""",
        (response_id,),
    )
