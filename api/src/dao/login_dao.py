from src.dao.dao_helper import cursor_handler
from src.config.logging_config import route_logger as logger


# CREATE TABLE login (
#     username    varchar(128),
#     passhash    varchar(128),
#     login_id    SERIAL PRIMARY KEY
#         CONSTRAINT fk_employee_login_id
#         REFERENCES employee(employee_id)
#         ON DELETE CASCADE
#         ON UPDATE CASCADE
# );

@cursor_handler
def create_login(login_id, username, passhash, cursor=None):
    cursor.execute(
        """INSERT INTO employee_login
                (login_id, username, passhash)
                VALUES (%s, %s, %s)
                RETURNING login_id, username, passhash""",
        (login_id, username, passhash),
    )
    result = cursor.fetchone()
    logger.info(f"create_login :: {result}")
    return result


@cursor_handler
def get_login_by_employee_id(empl_id, cursor=None):
    cursor.execute(
        """SELECT login_id, username, passhash FROM employee_login
                WHERE login_id = %s""",
        (empl_id,),
    )
    result = cursor.fetchone()
    logger.info(f"get_login_by_employee_id :: {result}")
    return result


@cursor_handler
def get_login_by_username(username, cursor=None):
    cursor.execute(
        """SELECT login_id, username, passhash FROM employee_login
                WHERE username LIKE %s""",
        (username,),
    )
    result = cursor.fetchone()
    logger.info(f"get_login_by_employee_id :: {result}")
    return result


@cursor_handler
def update_login(login_id, username, passhash, cursor=None):
    cursor.execute(
        """UPDATE employee_login
                SET username = %s, passhash = %s
                WHERE login_id = %s
                RETURNING login_id, username, passhash""",
        (username, passhash, login_id),
    )
    result = cursor.fetchone()
    logger.info(f"update_login :: {result}")
    return result


@cursor_handler
def delete_login(empl_id, cursor=None):
    cursor.execute(
        """DELETE FROM employee_login
                WHERE login_id = %s""",
        (empl_id,),
    )
