from src.dao.dao_helper import cursor_handler
from src.config.logging_config import route_logger as logger


@cursor_handler
def create_employee(api_key, employee_name, role_id, employee_id=-1, cursor=None):
    if employee_id >= 0:
        cursor.execute(
            """INSERT INTO employee
                    (employee_id, api_key, employee_name, role_id)
                    VALUES (%s, %s, %s, %s)
                    RETURNING employee_id, api_key, employee_name, role_id""",
            (employee_id, api_key, employee_name, role_id),
        )
    else:
        cursor.execute(
            """INSERT INTO employee
                    (api_key, employee_name, role_id)
                    VALUES (%s, %s, %s)
                    RETURNING employee_id, api_key, employee_name, role_id""",
            (api_key, employee_name, role_id),
        )
    result = cursor.fetchone()
    logger.info(f"create_employee :: {result}")
    return result


@cursor_handler
def get_all_employees(cursor):
    cursor.execute("""SELECT * FROM employee""")
    result = cursor.fetchall()
    logger.info(f"get_all_employees :: {result}")
    return result


@cursor_handler
def get_employee_by_id(empl_id, cursor):
    cursor.execute(
        """SELECT * FROM employee
                WHERE employee_id = %s""",
        (empl_id,),
    )
    result = cursor.fetchone()
    logger.info(f"get_employee_by_id :: {result}")
    return result


@cursor_handler
def get_employee_by_api_key(api_key, cursor):
    cursor.execute(
        """SELECT * FROM employee
                WHERE api_key = %s""",
        (api_key,),
    )
    result = cursor.fetchone()
    logger.info(f"get_employee_by_api_key :: {result}")
    return result


@cursor_handler
def update_employee(employee_id, api_key, employee_name, role_id, cursor):
    cursor.execute(
        """UPDATE employee
                SET api_key = %s, employee_name = %s, role_id = %s
                WHERE employee_id = %s
                RETURNING employee_id, api_key, employee_name, role_id""",
        (
            api_key,
            employee_name,
            role_id,
            employee_id,
        ),
    )
    result = cursor.fetchone()
    logger.info(f"update_employee :: {result}")
    return result


@cursor_handler
def delete_employee(empl_id, cursor):
    cursor.execute(
        """DELETE FROM employee
                WHERE employee_id = %s""",
        (empl_id,),
    )
