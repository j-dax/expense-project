from src.dao.dao_helper import cursor_handler
from src.config.logging_config import route_logger as logger

logger.name = "claim_file_dao"


@cursor_handler
def create_claim_file(c_file_id, file_path, mimetype, claim_id, cursor):
    if c_file_id < 0:
        cursor.execute(
            """INSERT INTO claim_file
                    (file_path, mimetype, claim_id)
                    VALUES (%s, %s, %s)
                    RETURNING c_file_id, file_path, mimetype, claim_id""",
            (file_path, mimetype, claim_id),
        )
    else:
        cursor.execute(
            """INSERT INTO claim_file
                    (c_file_id,file_path,mimetype,claim_id)
                    VALUES (%s, %s, %s, %s)
                    RETURNING c_file_id, file_path, mimetype, claim_id""",
            (c_file_id, file_path, mimetype, claim_id),
        )
    result = cursor.fetchone()
    logger.info(f"create_claim_file :: {result}")
    return result


@cursor_handler
def get_claim_file_by_id(c_file_id, cursor):
    cursor.execute(
        """SELECT * FROM claim_file
                WHERE c_file_id = %s""",
        (c_file_id,),
    )
    result = cursor.fetchone()
    logger.info(f"get_claim_file_by_id :: {result}")
    return result


@cursor_handler
def get_claim_file_by_cid_name(claim_id, file_path, cursor):
    cursor.execute(
        """SELECT * FROM claim_file
    WHERE claim_id = %s AND file_path LIKE %s""",
        (claim_id, file_path),
    )
    result = cursor.fetchone()
    logger.info(f"get_claim_file_by_cid_name :: {result}")
    return result


@cursor_handler
def get_files_by_claim_id(claim_id, cursor):
    cursor.execute(
        """SELECT * FROM claim_file
                WHERE claim_id = %s""",
        (claim_id,),
    )
    result = cursor.fetchall()
    logger.info(f"get_files_by_claim_id :: {result}")
    return result


@cursor_handler
def update_claim_file(c_file_id, file_path, mimetype, claim_id, cursor):
    cursor.execute(
        """UPDATE claim_file
                SET claim_id=%s, file_path=%s, mimetype=%s
                WHERE c_file_id = %s
                RETURNING c_file_id, file_path, mimetype, claim_id""",
        (claim_id, file_path, mimetype, c_file_id),
    )
    result = cursor.fetchone()
    logger.info(f"update_claim_file :: {result}")
    return result


@cursor_handler
def delete_claim_file_by_id(c_file_id, cursor):
    cursor.execute(
        """DELETE FROM claim_file
                WHERE c_file_id = %s""",
        (c_file_id,),
    )
