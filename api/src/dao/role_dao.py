from src.dao.dao_helper import cursor_handler
from src.config.logging_config import route_logger as logger

logger.name = "role_dao"


@cursor_handler
def create_role(role_id, perms, descr, cursor=None):
    cursor.execute(
        """INSERT INTO roles
                   (role_id, perms, descr)
                   VALUES (%s, %s, %s)
                   RETURNING role_id, perms, descr""",
        (role_id, perms, descr),
    )
    result = cursor.fetchone()
    logger.info(f"create_role :: {result}")
    return result


@cursor_handler
def get_role_by_id(role_id, cursor=None):
    cursor.execute(
        """SELECT * FROM roles
                   WHERE role_id = %s""",
        (role_id,),
    )
    result = cursor.fetchone()
    logger.info(f"get_role_by_id :: {result}")
    return result


@cursor_handler
def get_role_by_name(role_name, cursor=None):
    cursor.execute(
        """SELECT * FROM roles
                   WHERE perms = %s""",
        (role_name,),
    )
    result = cursor.fetchone()
    logger.info(f"get_role_by_name :: {result}")
    return result


@cursor_handler
def get_all_roles(cursor=None):
    cursor.execute("""SELECT * FROM roles""")
    result = cursor.fetchall()
    logger.info(f"get_all_roles :: {result}")
    return result


@cursor_handler
def update_role(role_id, perms, descr, cursor=None):
    cursor.execute(
        """UPDATE roles
                   SET perms = %s, descr = %s
                   WHERE role_id = %s
                   RETURNING role_id, perms, descr""",
        (perms, descr, role_id),
    )
    result = cursor.fetchone()
    logger.info(f"update_role :: {result}")
    return result


@cursor_handler
def delete_role_by_id(role_id, cursor=None):
    cursor.execute("""DELETE FROM roles WHERE role_id = %s""", (role_id,))
