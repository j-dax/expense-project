from src.dao import claim_file_dao
from src.model.claim import ClaimFile


def get_files_by_claim_id(claim_id):
    tpls = claim_file_dao.get_files_by_claim_id(claim_id)
    claims = [ClaimFile(*tpl) if tpl else None for tpl in tpls]
    for cf in claims:
        cf.direct_link = f"http://localhost:5000/api/claim/{claim_id}/upload/{cf.file_path}"
    return claims


def get_claim_file_by_cid_name(claim_id, name):
    tpl = claim_file_dao.get_claim_file_by_cid_name(claim_id, name)
    return ClaimFile(*tpl) if tpl else None


def enter_file(cf):
    # c_file_id, uri, claim_id):
    if cf.c_file_id >= 0:  # update existing
        tpl = claim_file_dao.update_claim_file(cf.c_file_id, cf.file_path, cf.mimetype, cf.claim_id)
    else:  # create new
        tpl = claim_file_dao.create_claim_file(-1, cf.file_path, cf.mimetype, cf.claim_id)
    return ClaimFile(*tpl) if tpl else None
