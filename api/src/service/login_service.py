from src.dao import login_dao
from src.model.login import Login


def get_login_by_username(username) -> Login:
    if not username:
        return None
    tpl = login_dao.get_login_by_username(username)
    login = Login(*tpl) if tpl else None
    return login
