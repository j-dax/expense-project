from src.model.claim import ClaimResponse
from src.dao import claim_resp_dao


def create_claim_response(resp_body, claim_id, cr_id=-1):
    tpl = claim_resp_dao.create_claim_response(resp_body, claim_id, cr_id)
    return ClaimResponse(*tpl) if tpl else None


def enter_claim_response(claim_response):
    cr_id = claim_resp_dao.get_claim_response_by_claim_id(claim_response.claim_id)
    # claim may not exist yet
    cr_id = cr_id[0] if cr_id else -1

    if cr_id > -1:
        # claim exists, update it
        # resp_id, resp_body, claim_id,
        tpl = claim_resp_dao.update_claim_response(cr_id, claim_response.resp_body, claim_response.claim_id)
        return ClaimResponse(*tpl) if tpl else None
    # claim doesn't exist, create it
    return create_claim_response(claim_response.resp_body, claim_response.claim_id)


def get_claim_response_by_claim_id(claim_id):
    tpl = claim_resp_dao.get_claim_response_by_claim_id(claim_id)
    return ClaimResponse(*tpl) if tpl else None
