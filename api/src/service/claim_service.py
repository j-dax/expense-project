from src.dao import claim_dao
from src.model.claim import Claim


def get_all_claims():
    tpls = claim_dao.get_all_claims()
    return list(filter(None, map(lambda x: Claim(*x) if x else None, tpls)))


def get_claims_by_employee_id(employee_id):
    tpls = claim_dao.get_claims_by_employee_id(employee_id)
    return list(filter(None, map(lambda x: Claim(*x) if x else None, tpls)))


def get_claim_by_claim_id(claim_id):
    tpl = claim_dao.get_claim_by_id(claim_id)
    return Claim(*tpl) if tpl else None


def enter_claim(claim):
    """Claim submission"""
    if claim.claim_id >= 0:
        # claim = Claim(-1, "", "PENDING", -1, "", employee.employee_id)
        # claim_id, claim_status, amount, claim_body, employee_id
        tpl = claim_dao.update_claim(
            claim.claim_id,
            claim.claim_status,
            claim.amount,
            claim.claim_body,
            claim.empl_id,
        )
    else:
        tpl = claim_dao.create_claim(claim.amount, claim.claim_body, claim.empl_id)
    return Claim(*tpl) if tpl else None


def create_claim(claim):
    tpl = claim_dao.create_claim(
        claim.amount, claim.claim_body, claim.empl_id, claim.claim_id
    )
    return Claim(*tpl) if tpl else None


def update_claim(claim):
    tpl = claim_dao.update_claim(
        claim.claim_id,
        claim.claim_status,
        claim.amount,
        claim.claim_body,
        claim.empl_id,
    )
    return Claim(*tpl) if tpl else None


def get_aggregated_report():
    data = claim_dao.claimant_data()
    if not data:
        data = []
    data = list(map(lambda x: {"employee_id": x[0], "claims_sum": float(x[1]), "claims_count": x[2]},
                    data))
    return {"data": data}
