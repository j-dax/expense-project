from src.dao import employee_dao, role_dao
from src.model.employee import Employee


def get_employee_by_id(empl_id) -> Employee:
    tpl = employee_dao.get_employee_by_id(empl_id)
    empl = Employee(*tpl[1:], tpl[0]) if tpl else None
    return empl


def get_employee_by_api_key(api_key) -> Employee:
    tpl = employee_dao.get_employee_by_api_key(api_key)
    empl = Employee(*tpl[1:], tpl[0]) if tpl else None
    return empl
