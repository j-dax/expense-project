from src.config.db_config import get_connection
from src.model.role import RoleFactory
from src.dao import role_dao, employee_dao


def get_role_by_employee_id(empl_id):
    empl = employee_dao.get_employee_by_id(empl_id)
    return get_role_by_role_id(empl[3])


def get_role_by_role_id(role_id):
    role = role_dao.get_role_by_id(role_id)
    return RoleFactory.get_role(role[1])
