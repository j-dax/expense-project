from unittest import TestCase, mock
from src.config.flask_config import app
from src.model.employee import Employee
from src.service import employee_service


class TestEmployeeService(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = app.test_client()

    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @mock.patch("src.dao.employee_dao.get_employee_by_id")
    def test_get_employee_by_id(self, m_get_empl_by_id):
        # get_employee_by_id(empl_id) -> Employee:
        employees = [
            (100, "JimJohnsAPIKEY", "Jim John", 0),
            (101, "JaniesAPIKEY", "Jane John", 1)
        ]

        for empl in employees:
            m_get_empl_by_id.return_value = empl
            result = employee_service.get_employee_by_id(empl[0])
            self.assertEqual(Employee(*empl[1:] + (empl[0],)), result)

    @mock.patch("src.dao.employee_dao.get_employee_by_api_key")
    def test_get_employee_by_api_key(self, m_get_empl_by_key):
        # get_employee_by_api_key(api_key) -> Employee:
        employees = [
            (100, "JimJohnsAPIKEY", "Jim John", 0),
            (101, "JaniesAPIKEY", "Jane John", 1)
        ]

        for empl in employees:
            m_get_empl_by_key.return_value = empl
            result = employee_service.get_employee_by_api_key(empl[1])
            self.assertEqual(Employee(*empl[1:] + (empl[0],)), result)
