from unittest import TestCase, mock
from src.model.claim import ClaimResponse
from src.config.flask_config import app
from src.service import claim_resp_service


class TestClaimResponseService(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = app.test_client()

    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @mock.patch("src.dao.claim_resp_dao.create_claim_response")
    def test_create_claim_response(self, m_create_response):
        # create_claim_response(resp_body, claim_id, cr_id=-1)
        responses = [
            (2000, "a", "", 100),
            (2001, "v", "", 101),
            (2002, "g", "", 102)
        ]
        for resp in responses:
            m_create_response.return_value = resp
            result = claim_resp_service.create_claim_response(resp[1], resp[3])
            self.assertIsInstance(result, ClaimResponse)

    @mock.patch("src.dao.claim_resp_dao.get_claim_response_by_claim_id")
    @mock.patch("src.dao.claim_resp_dao.create_claim_response")
    def test_enter_claim_response(self, m_create_response, m_get_response):
        # enter_claim_response(claim_response)
        responses = [
            (2000, "a", "", 100),
            (2001, "v", "", 101),
            (2002, "g", "", 102)
        ]
        m_get_response.return_value = None

        for resp in responses:
            m_create_response.return_value = resp
            result = claim_resp_service.enter_claim_response(ClaimResponse(*resp))
            self.assertIsInstance(result, ClaimResponse)

    @mock.patch("src.dao.claim_resp_dao.get_claim_response_by_claim_id")
    def test_get_claim_response_by_claim_id(self, m_get_response_by_claim_id):
        # get_claim_response_by_claim_id(claim_id):
        responses = [
            (2000, "a", "", 100),
            (2001, "v", "", 101),
            (2002, "g", "", 102)
        ]

        for resp in responses:
            m_get_response_by_claim_id.return_value = resp
            result = claim_resp_service.get_claim_response_by_claim_id(ClaimResponse(*resp))
            self.assertIsInstance(result, ClaimResponse)
