from unittest import TestCase, mock
from src.model.claim import ClaimFile
from src.config.flask_config import app
from src.service import claim_file_service


class TestClaimFileService(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = app.test_client()

    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @mock.patch("src.dao.claim_file_dao.get_files_by_claim_id")
    def test_get_files_by_claim_id(self, m_get_file_by_claim_id):
        # get_files_by_claim_id(claim_id):
        files = [
            # c_file_id, file_path, mimetype, claim_id
            (3000, "a", "t", 100),
            (3001, "b", "t", 100)
        ]
        m_get_file_by_claim_id.return_value = files
        fs = claim_file_service.get_files_by_claim_id(files[0][3])
        self.assertTrue(len(fs[0].direct_link) > 0)
        self.assertIsInstance(fs[0], ClaimFile)
        self.assertTrue(len(fs[1].direct_link) > 0)
        self.assertIsInstance(fs[1], ClaimFile)

    @mock.patch("src.dao.claim_file_dao.get_claim_file_by_cid_name")
    def test_get_claim_file_by_cid_name(self, m_get_file_by_name):
        # get_claim_file_by_cid_name(claim_id, name):
        files = [
            # c_file_id, file_path, mimetype, claim_id
            (3000, "a", "t", 100),
            (3001, "b", "t", 100)
        ]
        for f in files:
            m_get_file_by_name.return_value = f
            fi = claim_file_service.get_claim_file_by_cid_name(f[3], f[1])
            self.assertIsInstance(fi, ClaimFile)

    @mock.patch("src.dao.claim_file_dao.update_claim_file")
    @mock.patch("src.dao.claim_file_dao.create_claim_file")
    def test_enter_file(self, m_create_file, m_update_file):
        # enter_file(cf):
        files = [
            # c_file_id, file_path, mimetype, claim_id
            (3000, "a", "t", 100),
            (3001, "b", "t", 100)
        ]
        for f in files:
            m_update_file.return_value = f
            fi = claim_file_service.enter_file(ClaimFile(*f))
            self.assertIsInstance(fi, ClaimFile)

        files = [
            (-1, "b", "t", 100)
        ]
        m_create_file.return_value = (2000, "b", "t", 100)
        fi = claim_file_service.enter_file(ClaimFile(*files[0]))
        self.assertGreaterEqual(fi.c_file_id, 0)
