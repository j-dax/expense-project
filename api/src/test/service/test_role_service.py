from unittest import TestCase, mock
from src.config.flask_config import app
from src.model.role import ManagerRole, EmployeeRole
from src.service import role_service


class TestEmployeeService(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = app.test_client()

    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @mock.patch('src.dao.employee_dao.get_employee_by_id')
    @mock.patch("src.dao.role_dao.get_role_by_id")
    def test_get_role_by_employee_id(self, m_get_role_by_id, m_get_employee_by_id):
        # get_role_by_employee_id(empl_id):
        employees = [
            ("", "", 0, 100),
            ("", "", 1, 101)
        ]
        roles = [
            # role_id, perms, descr
            (0, "MGR", "manager"),
            (1, "EMP", "employee")
        ]
        role_class = [
            ManagerRole,
            EmployeeRole
        ]
        for emp, role, cls in zip(employees, roles, role_class):
            m_get_employee_by_id.return_value = emp
            m_get_role_by_id.return_value = role
            result = role_service.get_role_by_role_id(role[0])
            self.assertIsInstance(result, cls)

    @mock.patch("src.dao.role_dao.get_role_by_id")
    def test_get_role_by_role_id(self, m_role_by_id):
        # get_role_by_role_id(role_id):
        roles = [
            # role_id, perms, descr
            (0, "MGR", "manager"),
            (1, "EMP", "employee")
        ]
        role_class = [
            ManagerRole,
            EmployeeRole
        ]
        for k, cls in zip(roles, role_class):
            m_role_by_id.return_value = k
            result = role_service.get_role_by_role_id(k[0])
            self.assertIsInstance(result, cls)
