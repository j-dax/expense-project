from unittest import TestCase, mock
from hashlib import sha512
from os import environ
from src.config.flask_config import app
from src.model.login import Login
from src.service import login_service


class TestEmployeeService(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = app.test_client()

    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @mock.patch('src.dao.login_dao.get_login_by_username')
    def test_get_login_by_username(self, m_login_by_username):
        # get_login_by_username(username) -> Login:
        # login_id, username, passhash
        logins = [
            (100, "admin", sha512("admin".encode()).hexdigest()),
            (101, "user", sha512("presalt".encode()).hexdigest())
        ]

        # m_login_by_username.side_effects = [
        #     logins[0],
        #     logins[1]
        # ]

        for tpl in logins:
            m_login_by_username.return_value = tpl
            login = login_service.get_login_by_username(tpl[1])
            self.assertEqual(Login(*tpl), login)

        self.assertIsNone(login_service.get_login_by_username(""))
