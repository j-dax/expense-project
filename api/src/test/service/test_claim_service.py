from unittest import TestCase, mock
from src.config.flask_config import app
from src.model.claim import Claim
from src.service import claim_service


class TestClaimService(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = app.test_client()

    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @mock.patch("src.dao.claim_dao.get_all_claims")
    def test_get_all_claims(self, m_get_all_claims):
        claims = [
            # (claim_id, last_update, claim_status, amount, claim_body, empl_id)
            (1000, "", "PENDING", 1.00, "", 100),
            (1001, "", "PENDING", 2.00, "", 100),
            (1002, "", "PENDING", 20.00, "", 100),
            (1003, "", "PENDING", 40.00, "", 102),
            (1004, "", "PENDING", 80.00, "", 102)
        ]
        m_get_all_claims.return_value = claims
        for claim in claim_service.get_all_claims():
            self.assertIsInstance(claim, Claim)

    @mock.patch("src.dao.claim_dao.get_claims_by_employee_id")
    def test_get_claims_by_employee_id(self, m_get_claims_by_empl_id):
        # get_claims_by_employee_id(employee_id):
        claims = [
            # (claim_id, last_update, claim_status, amount, claim_body, empl_id)
            (1000, "", "PENDING", 1.00, "", 100),
            (1001, "", "PENDING", 2.00, "", 100),
            (1002, "", "PENDING", 20.00, "", 100)
        ]
        m_get_claims_by_empl_id.return_value = claims
        for claim in claim_service.get_claims_by_employee_id(100):
            self.assertIsInstance(claim, Claim)
            self.assertEqual(claim.empl_id, 100)

    @mock.patch("src.dao.claim_dao.get_claim_by_id")
    def test_get_claim_by_claim_id(self, m_get_claim_by_claim_id):
        # get_claim_by_claim_id(claim_id):
        claims = [
            # (claim_id, last_update, claim_status, amount, claim_body, empl_id)
            (1000, "", "PENDING", 1.00, "", 100),
            (1001, "", "PENDING", 2.00, "", 100),
            (1002, "", "PENDING", 20.00, "", 100)
        ]
        m_get_claim_by_claim_id.side_effect = claims
        for claim in claims:
            result = claim_service.get_claim_by_claim_id(claim[0])
            self.assertIsInstance(result, Claim)
            self.assertEqual(result.claim_id, claim[0])

    @mock.patch("src.dao.claim_dao.update_claim")
    @mock.patch("src.dao.claim_dao.create_claim")
    def test_enter_claim(self, m_update_claim, m_create_claim):
        # enter_claim(claim):
        claims = [
            # (claim_id, last_update, claim_status, amount, claim_body, empl_id)
            (1000, "", "PENDING", 1.00, "", 100),
            (1001, "", "PENDING", 2.00, "", 100),
            (-1, "", "PENDING", 20.00, "", 100)
        ]
        m_update_claim.side_effect = claims[:2]
        m_create_claim.return_value = claims[2]
        for claim in claims:
            result = claim_service.enter_claim(Claim(*claim))
            self.assertIsInstance(result, Claim)

    @mock.patch('src.dao.claim_dao.create_claim')
    def test_create_claim(self, m_create_claim):
        # create_claim(claim):
        claims = [
            # (claim_id, last_update, claim_status, amount, claim_body, empl_id)
            (1000, "", "PENDING", 1.00, "", 100),
            (1001, "", "PENDING", 2.00, "", 100),
            (-1, "", "PENDING", 20.00, "", 100)
        ]
        m_create_claim.side_effect = claims
        for claim in claims:
            result = claim_service.create_claim(Claim(*claim))
            self.assertIsInstance(result, Claim)

    @mock.patch('src.dao.claim_dao.update_claim')
    def test_update_claim(self, m_update_claim):
        # update_claim(claim):
        claims = [
            # (claim_id, last_update, claim_status, amount, claim_body, empl_id)
            (1000, "", "PENDING", 1.00, "", 100),
            (1001, "", "PENDING", 2.00, "", 100),
            (-1, "", "PENDING", 20.00, "", 100)
        ]
        m_update_claim.side_effect = claims
        for claim in claims:
            result = claim_service.update_claim(Claim(*claim))
            self.assertIsInstance(result, Claim)
