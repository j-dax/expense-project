import unittest
from src.config.db_config import get_connection, reset_database
from src.dao import claim_resp_dao


class ClaimRespDAOTest(unittest.TestCase):
    def setUp(self):
        super().setUp()

        reset_database()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.executemany(
                    """INSERT INTO roles (role_id, perms, descr)
                                VALUES (%s, %s, %s)""",
                    ((0, "EMP", "Employee"), (1, "MGR", "Manager")),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO employee (employee_id, api_key, employee_name, role_id)
                                VALUES (%s, %s, %s, %s)""",
                    (
                        (500, "can", "a", 0),
                        (501, "do", "b", 1),
                        (502, "attitude", "b", 1),
                    ),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO claim (claim_id,last_update,claim_status,amount,claim_body,filed_by)
                                VALUES (%s, default, %s, %s, %s, %s)""",
                    (
                        (400, "PENDING", 40.0, "no body", 500),
                        (401, "APPROVED", 400.0, "no body", 501),
                        (402, "REJECTED", 4000.0, "no body", 502),
                    ),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO claim_response (response_id, resp_body, last_update, claim_id)
                                VALUES (%s, %s, default, %s)""",
                    (
                        (410, "no resp", 400),
                        (411, "no resp", 401),
                        (412, "no resp", 402),
                    ),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO claim_file (c_file_id, file_path, mimetype, claim_id)
                                VALUES (%s, %s, %s, %s)""",
                    (
                        (4000, "400.txt", "text/plain", 400),
                        (4001, "400.json", "application/json", 400),
                        (4002, "400.yml", "application/yaml", 400),
                        (4010, "401.txt", "text/plain", 401),
                        (4011, "401.json", "application/json", 401),
                        (4012, "401.yml", "application/yaml", 401),
                        (4020, "402.txt", "text/plain", 402),
                        (4021, "402.json", "application/json", 402),
                        (4022, "402.yml", "application/yaml", 402),
                    ),
                )
                conn.commit()

    def tearDown(self):
        super().tearDown()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """TRUNCATE claim_file, claim_response, claim, employee, roles CASCADE"""
                )
                conn.commit()

    def test_create_claim_response(self):
        claim_resp = [
            (600, "no resp", 400),
            (601, "no resp", 401),
            (602, "no resp", 402),
        ]
        for resp in claim_resp:
            new_resp = claim_resp_dao.create_claim_response(resp[1], resp[2], resp[0])
            self.assertEqual(new_resp[1], resp[1])
            self.assertEqual(new_resp[-1], resp[-1])

    def test_get_claim_response(self):
        claim_resp = [
            (410, "no resp", 400),
            (411, "no resp", 401),
            (412, "no resp", 402),
        ]

        for resp in claim_resp:
            found_resp = claim_resp_dao.get_claim_response(resp[0])
            found_resp = found_resp[:2] + found_resp[3:]
            self.assertEqual(found_resp, resp)

    def test_update_claim_response(self):
        claim_resp = [
            (410, "yup, that's a response", 400),
            (411, "message updated", 401),
            (412, "right there", 402),
        ]
        for cr in claim_resp:
            claim_resp_dao.update_claim_response(cr[0], cr[1], cr[2])
            # self.assertEqual(cr, updated)
            self.assertTrue(True)

    def test_delete_claim_response(self):
        claim_resp = [
            (410, "no resp", 400),
            (411, "no resp", 401),
            (412, "no resp", 402),
        ]
        for cr in claim_resp:
            claim_resp_dao.delete_claim_response(cr[0])
            self.assertIsNone(claim_resp_dao.get_claim_response(cr[0]))
