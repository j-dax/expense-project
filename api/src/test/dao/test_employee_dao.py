import unittest
from src.dao import employee_dao
from src.config.db_config import get_connection, reset_database


class EmployeeDAOTest(unittest.TestCase):
    def setUp(self):
        super().setUp()

        reset_database()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.executemany("""INSERT INTO roles (role_id, perms, descr)
                                VALUES (%s, %s, %s)""",
                                ((0, "EMP", "Employee"),
                                 (1, "MGR", "Manager")))
                conn.commit()
                cur.executemany("""INSERT INTO employee (employee_id, api_key, employee_name, role_id)
                                VALUES (%s, %s, %s, %s)""",
                                ((500, "abc", "a", 0),
                                 (501, "def", "b", 1),
                                 (502, "ghi", "c", 1)))
                conn.commit()

    def tearDown(self):
        super().tearDown()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""TRUNCATE claim_file, claim_response, claim, employee, roles CASCADE""")
                conn.commit()

    def test_create_employee(self):
        employees = [
            (600, "jkl", "a", 0),
            (601, "lmn", "b", 1),
            (602, "opq", "c", 1)
        ]
        for emp in employees:
            new_emp = employee_dao.create_employee(emp[1], emp[2], emp[3])
            self.assertEqual(emp[1:], new_emp[1:])

    def test_get_all_employees(self):
        self.assertGreater(len(employee_dao.get_all_employees()), 2)

    def test_get_employee_by_id(self):
        employees = [
            (500, "abc", "a", 0),
            (501, "def", "b", 1),
            (502, "ghi", "c", 1)
        ]
        for emp in employees:
            self.assertEqual(emp, employee_dao.get_employee_by_id(emp[0]))

    def test_update_employee(self):
        employees = [
            (500, "abc", "a", 0),
            (501, "def", "b", 1),
            (502, "ghi", "c", 1)
        ]
        for e in employees:
            emp = employee_dao.update_employee(*e)
            self.assertEqual(emp, e)

    def test_delete_employee(self):
        employees = [
            (500, "abc", "a", 0),
            (501, "def", "b", 1),
            (502, "ghi", "c", 1)
        ]
        for e in employees:
            employee_dao.delete_employee(e[0])
            self.assertIsNone(employee_dao.get_employee_by_id(e[0]))
