import unittest
from src.dao import login_dao, employee_dao
from src.config.db_config import get_connection, reset_database
from hashlib import sha512


class EmployeeDAOTest(unittest.TestCase):
    def setUp(self):
        super().setUp()

        reset_database()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.executemany(
                    """INSERT INTO roles (role_id, perms, descr)
                                VALUES (%s, %s, %s)""",
                    ((0, "EMP", "Employee"), (1, "MGR", "Manager")),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO employee (employee_id, api_key, employee_name, role_id)
                                VALUES (%s, %s, %s, %s)""",
                    ((500, "abc", "a", 0), (501, "def", "b", 1), (502, "ghi", "c", 1)),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO employee_login (login_id, username, passhash)
                                VALUES (%s, %s, %s)""",
                    (
                        (500, "user1", sha512("pass1".encode()).hexdigest()),
                        (501, "user2", sha512("pass2".encode()).hexdigest()),
                        (502, "user3", sha512("pass3".encode()).hexdigest()),
                    ),
                )

    def tearDown(self):
        super().tearDown()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """TRUNCATE claim_file, claim_response, claim, employee_login, employee, roles CASCADE"""
                )
                conn.commit()

    def test_create_login(self):
        employees = [
            (600, "jkl", "a", 0),
            (601, "lmn", "b", 1),
            (602, "opq", "c", 1)]
        logins = [
            (600, "user1", sha512("pass1".encode()).hexdigest()),
            (601, "user2", sha512("pass2".encode()).hexdigest()),
            (602, "user3", sha512("pass3".encode()).hexdigest())]

        for emp, lg in zip(employees, logins):
            self.assertIsNotNone(employee_dao.create_employee(emp[1], emp[2], emp[3], emp[0]))
            login = login_dao.create_login(*lg)
            self.assertEqual(lg, login)

    def test_get_login_by_employee_id(self):
        logins = [
            (500, "user1", sha512("pass1".encode()).hexdigest()),
            (501, "user2", sha512("pass2".encode()).hexdigest()),
            (502, "user3", sha512("pass3".encode()).hexdigest())]
        for lg in logins:
            self.assertEqual(lg, login_dao.get_login_by_employee_id(lg[0]))

    def test_get_login_by_username(self):
        # get_login_by_username(username
        logins = [
            (500, "user1", sha512("pass1".encode()).hexdigest()),
            (501, "user2", sha512("pass2".encode()).hexdigest()),
            (502, "user3", sha512("pass3".encode()).hexdigest())]
        for lg in logins:
            self.assertEqual(lg, login_dao.get_login_by_username(lg[1]))

    def test_update_employee(self):
        logins = [
            (500, "user1", sha512("123456".encode()).hexdigest()),
            (501, "user2", sha512("7890ab".encode()).hexdigest()),
            (502, "user3", sha512("cdefgh".encode()).hexdigest())]
        for lg in logins:
            self.assertEqual(login_dao.update_login(*lg)[-1], lg[-1])

    def test_delete_employee(self):
        logins = [
            (500, "user1", sha512("123456".encode()).hexdigest()),
            (501, "user2", sha512("7890ab".encode()).hexdigest()),
            (502, "user3", sha512("cdefgh".encode()).hexdigest())]
        for lg in logins:
            login_dao.delete_login(lg[0])
            self.assertIsNone(login_dao.get_login_by_employee_id(lg[0]))
