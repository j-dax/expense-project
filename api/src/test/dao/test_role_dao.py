import unittest
from src.dao import role_dao
from src.config.db_config import get_connection, reset_database


class RoleDAOTest(unittest.TestCase):
    def setUp(self):
        super().setUp()

        reset_database()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.executemany("""INSERT INTO roles (role_id, perms, descr)
                                VALUES (%s, %s, %s)""",
                                ((0, "EMP", "Employee"),
                                 (1, "MGR", "Manager")))
                conn.commit()

    def tearDown(self):
        super().tearDown()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""TRUNCATE roles CASCADE""")
                conn.commit()

    def test_create_role(self):
        roles = [
            (2, "PRB", "Probation"),
            (3, "CAM", "SleepWalkin")
        ]

        for r in roles:
            new_r = role_dao.create_role(*r)
            self.assertEqual(r, new_r)

    def test_get_all_roles(self):
        self.assertGreaterEqual(len(role_dao.get_all_roles()), 2)

    def test_get_employee_by_id(self):
        roles = [
            (0, "EMP", "Employee"),
            (1, "MGR", "Manager")
        ]
        for r in roles:
            self.assertEqual(r, role_dao.get_role_by_id(r[0]))

    def test_update_employee(self):
        roles = [
            (0, "EMP", "Emp"),
            (1, "MGR", "Mgr")
        ]
        for r in roles:
            upd_r = role_dao.update_role(*r)
            self.assertEqual(upd_r, r)

    def test_delete_employee(self):
        roles = [
            (0, "EMP", "Employee"),
            (1, "MGR", "Manager")
        ]
        for r in roles:
            role_dao.delete_role_by_id(r[0])
            self.assertIsNone(role_dao.get_role_by_id(r[0]))
