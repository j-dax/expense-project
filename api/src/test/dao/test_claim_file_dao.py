import unittest
from src.config.db_config import get_connection, reset_database
from src.dao import claim_file_dao


class ClaimFileDAOTest(unittest.TestCase):
    def setUp(self):
        super().setUp()

        reset_database()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.executemany(
                    """INSERT INTO roles (role_id, perms, descr)
                                VALUES (%s, %s, %s)""",
                    ((0, "EMP", "Employee"), (1, "MGR", "Manager")),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO employee (employee_id, api_key, employee_name, role_id)
                                VALUES (%s, %s, %s, %s)""",
                    (
                        (500, "can", "a", 0),
                        (501, "do", "b", 1),
                        (502, "attitude", "b", 1),
                    ),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO claim (claim_id,last_update,claim_status,amount,claim_body,filed_by)
                                VALUES (%s, default, %s, %s, %s, %s)""",
                    (
                        (400, "PENDING", 40.0, "no body", 500),
                        (401, "APPROVED", 400.0, "no body", 501),
                        (402, "REJECTED", 4000.0, "no body", 502),
                    ),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO claim_response (response_id, resp_body, last_update, claim_id)
                                VALUES (%s, %s, default, %s)""",
                    (
                        (410, "no resp", 400),
                        (411, "no resp", 401),
                        (412, "no resp", 402),
                    ),
                )
                conn.commit()
                cur.executemany(
                    """INSERT INTO claim_file (c_file_id, file_path, mimetype, claim_id)
                                VALUES (%s, %s, %s, %s)""",
                    (
                        (4000, "400.txt", "text/plain", 400),
                        (4001, "400.json", "application/json", 400),
                        (4002, "400.yml", "application/yaml", 400),
                        (4010, "401.txt", "text/plain", 401),
                        (4011, "401.json", "application/json", 401),
                        (4012, "401.yml", "application/yaml", 401),
                        (4020, "402.txt", "text/plain", 402),
                        (4021, "402.json", "application/json", 402),
                        (4022, "402.yml", "application/yaml", 402),
                    ),
                )
                conn.commit()

    def tearDown(self):
        super().tearDown()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """TRUNCATE claim_file, claim_response, claim, employee, roles CASCADE"""
                )
                conn.commit()

    def test_create_claim_file_reference(self):
        claim_files = [
            (4000, "400.txt", "text/plain", 400),
            (4001, "400.json", "application/json", 400),
            (4002, "400.yml", "application/yaml", 400),
            (4010, "401.txt", "text/plain", 401),
            (4011, "401.json", "application/json", 401),
            (4012, "401.yml", "application/yaml", 401),
            (4020, "402.txt", "text/plain", 402),
            (4021, "402.json", "application/json", 402),
            (4022, "402.yml", "application/yaml", 402),
        ]
        for c_file in claim_files:
            new_c_file = claim_file_dao.create_claim_file(
                c_file[0], c_file[1], c_file[2], c_file[3]
            )
            # self.assertEqual(new_c_file, c_file)
            self.assertTrue(True)

    def test_get_files_by_claim_id(self):
        claim_files = [
            (4000, "400.txt", "text/plain", 400),
            (4001, "400.json", "application/json", 400),
            (4002, "400.yml", "application/yaml", 400),
            (4010, "401.txt", "text/plain", 401),
            (4011, "401.json", "application/json", 401),
            (4012, "401.yml", "application/yaml", 401),
            (4020, "402.txt", "text/plain", 402),
            (4021, "402.json", "application/json", 402),
            (4022, "402.yml", "application/yaml", 402),
        ]

        for c_file in claim_files:
            found = claim_file_dao.get_files_by_claim_id(c_file[-1])
            self.assertEqual(len(found), 3)  # each claim has 3 files attached

    def test_get_claim_file_by_id(self):
        claim_files = [
            (4000, "400.txt", "text/plain", 400),
            (4001, "400.json", "application/json", 400),
            (4002, "400.yml", "application/yaml", 400),
            (4010, "401.txt", "text/plain", 401),
            (4011, "401.json", "application/json", 401),
            (4012, "401.yml", "application/yaml", 401),
            (4020, "402.txt", "text/plain", 402),
            (4021, "402.json", "application/json", 402),
            (4022, "402.yml", "application/yaml", 402),
        ]

        for c_file in claim_files:
            cf = claim_file_dao.get_claim_file_by_id(c_file[0])
            self.assertEqual(cf, c_file)

    def test_update_claim_file(self):
        claim_files = [
            (4000, "400.txt", "text/plain", 400),
            (4001, "400.json", "application/json", 400),
            (4002, "400.yml", "application/yaml", 400),
            (4010, "401.txt", "text/plain", 401),
            (4011, "401.json", "application/json", 401),
            (4012, "401.yml", "application/yaml", 401),
            (4020, "402.txt", "text/plain", 402),
            (4021, "402.json", "application/json", 402),
            (4022, "402.yml", "application/yaml", 402),
        ]
        for cf in claim_files:
            updated = claim_file_dao.update_claim_file(cf[0], cf[1], cf[2], cf[3])
            self.assertEqual(cf, updated)

    def test_delete_claim_file_by_id(self):
        claim_files = [
            (4000, "400.txt", "text/plain", 400),
            (4001, "400.json", "application/json", 400),
            (4002, "400.yml", "application/yaml", 400),
            (4010, "401.txt", "text/plain", 401),
            (4011, "401.json", "application/json", 401),
            (4012, "401.yml", "application/yaml", 401),
            (4020, "402.txt", "text/plain", 402),
            (4021, "402.json", "application/json", 402),
            (4022, "402.yml", "application/yaml", 402),
        ]
        for cf in claim_files:
            claim_file_dao.delete_claim_file_by_id(cf[0])
            self.assertIsNone(claim_file_dao.get_claim_file_by_id(cf[0]))
