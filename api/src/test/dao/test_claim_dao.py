import unittest
from src.config.db_config import get_connection, reset_database
from src.dao import claim_dao


class ClaimDAOTest(unittest.TestCase):
    def setUp(self):
        super().setUp()

        reset_database()
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.executemany("""INSERT INTO roles (role_id, perms, descr)
                                VALUES (%s, %s, %s)""",
                                ((0, "EMP", "Employee"),
                                 (1, "MGR", "Manager")))
                conn.commit()
                cur.executemany("""INSERT INTO employee (employee_id, api_key, employee_name, role_id)
                                VALUES (%s, %s, %s, %s)""",
                                ((500, "can", "a", 0),
                                 (501, "do", "b", 1),
                                 (502, "attitude", "b", 1)))
                conn.commit()
                cur.executemany("""INSERT INTO claim (claim_id,last_update,claim_status,amount,claim_body,filed_by)
                                VALUES (%s, default, %s, %s, %s, %s)""",
                                ((400, "PENDING", 40.0, "no body", 500),
                                 (401, "APPROVED", 400.0, "no body", 501),
                                 (402, "REJECTED", 4000.0, "no body", 502)))
                conn.commit()

    def tearDown(self):
        super().tearDown()
        
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""TRUNCATE claim_file, claim_response, claim, employee, roles CASCADE""")
                conn.commit()

    def test_create_claim(self):
        # def create_claim(amount, claim_body, empl_id, claim_id=-1):
        # (400, "PENDING", 40.0, "no body", 500),
        claim_tpl = claim_dao.create_claim(40.0, "no body", 502)
        self.assertEqual(claim_tpl[2], "PENDING")
        self.assertEqual(float(claim_tpl[3]), 40.00)
        self.assertEqual(claim_tpl[4], "no body")
        self.assertEqual(claim_tpl[5], 502)
        # duplicate ids return None
        self.assertEqual(claim_dao.create_claim(40.0, "no body", 502, claim_id=claim_tpl[0]), None)

    def test_get_all_claims(self):
        # def get_all_claims():
        claim_tpls = claim_dao.get_all_claims()
        # setup inserts 3 records
        self.assertGreater(len(claim_tpls), 2)

    def test_get_claim_by_id(self):
        # def get_claim_by_id(claim_id):
        claims_inserted = [
            (400, "PENDING", 40.0, "no body", 500),
            (401, "APPROVED", 400.0, "no body", 501),
            (402, "REJECTED", 4000.0, "no body", 502)
        ]
        claims_from_dao = [
            claim_dao.get_claim_by_id(400),
            claim_dao.get_claim_by_id(401),
            claim_dao.get_claim_by_id(402)
        ]
        # remove additional column, datetime @ index 1
        for i in range(len(claims_from_dao)):
            tpl = claims_from_dao[i]
            tpl = (tpl[0], ) + tpl[2:]
            claims_from_dao[i] = tpl

        for dao, inserted in zip(claims_from_dao, claims_inserted):
            self.assertEqual(dao, inserted)

    def test_update_claim(self):
        # def update_claim(claim):
        claims = [
            (400, "PENDING", 41.0, "a", 500),
            (401, "APPROVED", 410.0, "b", 501),
            (402, "REJECTED", 4100.0, "c", 502)
        ]
        for claim in claims:
            updated_claim = claim_dao.update_claim(claim[0], claim[1], claim[2], claim[3], claim[4])
            # discard timestamp from test
            updated_claim = (updated_claim[0],) + updated_claim[2:] 
            self.assertEqual(updated_claim, claim)

    def test_delete_claim(self):
        claims = [
            (400, "PENDING", 41.0, "a", 500),
            (401, "APPROVED", 410.0, "b", 501),
            (402, "REJECTED", 4100.0, "c", 502)
        ]
        for claim in claims:
            claim_dao.delete_claim_by_id(claim[0])
            self.assertEqual(claim_dao.get_claim_by_id(claim[0]), None)
