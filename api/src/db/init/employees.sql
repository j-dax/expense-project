TRUNCATE roles CASCADE;
INSERT INTO roles (role_id, perms, descr) VALUES
    (0, 'MGR', 'Manager'),
    (1, 'EMP', 'Employee');

TRUNCATE employee CASCADE;
INSERT INTO employee (employee_id, api_key, employee_name, role_id)
	VALUES 
    (1, 'totallyuniqueapikey1', 'a', 0),
    (2, 'totallyuniqueapikey2', 'c', 1),
    (3, 'totallyuniqueapikey3', 'b', 1),
    (4, 'totallyuniqueapikey4', 'd', 1);

TRUNCATE employee_login;
INSERT INTO employee_login (username, passhash, login_id)
    VALUES
    -- admin:admin
    ('admin','c02e18b398b6c77095e14b1877fbf0697184fd88d7c7673a5a68e799435cac8c1f728cdc3e291cc15bec14db43d0df69d6b99a51f32b1c2917a326bc6cc399ff',1),
    -- ryan:sqlite
    ('ryan','0cb877f97db4caf0b995f78dc8f92d5c92ba0b8638bb69d836071a8fe8ebe96013b755032629b712e0b4bb66aa7381b8ab761602b9563e782894c002b484de67',2),
    -- jameson:gambler
    ('jameson','3837ea3f82a94ea2422430c0647aa363b8be6f391dff56a7054804b38c14535cb4d45d102fadcc1fd91414b06a4ab764b33e555ccf68a9291a18c41404741e74',3),
    -- zach:strimmer
    ('zach','6406df24695ca175c2b313579095a4ec2cb798e2d8d537112269a390489ef3794ff986c04eea86883f4d6f1dcad417a84528a4cc50046b8ed0fc44cad1a62143',4);

TRUNCATE claim CASCADE;
INSERT INTO claim (claim_id, last_update, claim_status, amount, claim_body, filed_by)
    VALUES
    -- admin
    (0, DEFAULT, DEFAULT, 1.0, 'test', 1),
    -- ryan
    (1, DEFAULT, DEFAULT, 10.0, 'iterate', 2),
    (2, DEFAULT, DEFAULT, 100.0, 'wusyaname', 2),
    -- jameson
    (3, DEFAULT, DEFAULT, 20.0, 'hyuck', 3),
    (4, DEFAULT, DEFAULT, 200.0, 'pool', 3),
    (5, DEFAULT, DEFAULT, 2000.0, 'say it aint so', 3),
    -- zach
    (6, DEFAULT, DEFAULT, 30.0, 'sussy', 4),
    (7, DEFAULT, DEFAULT, 300.0, 'dumpies', 4),
    (8, DEFAULT, DEFAULT, 3000.0, 'haHAH', 4),
    (9, DEFAULT, DEFAULT, 30000.0, 'strimmingpcpls', 4);

TRUNCATE claim_response;
INSERT INTO claim_response (response_id, resp_body, last_update, claim_id)
    VALUES
    (0, 'test body', DEFAULT, 0);

TRUNCATE claim_file;
INSERT INTO claim_file (c_file_id, file_path, claim_id)
    VALUES
    (0, 'test body', 0);