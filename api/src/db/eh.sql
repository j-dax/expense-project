
TRUNCATE claim CASCADE;
INSERT INTO claim (claim_id, last_update, claim_status, amount, claim_body, filed_by)
    VALUES
    -- admin
    (0, DEFAULT, DEFAULT, 1.0, 'test', 1),
    -- ryan
    (1, DEFAULT, DEFAULT, 10.0, 'iterate', 2),
    (2, DEFAULT, DEFAULT, 100.0, 'wusyaname', 2),
    -- jameson
    (3, DEFAULT, DEFAULT, 20.0, 'hyuck', 3),
    (4, DEFAULT, DEFAULT, 200.0, 'pool', 3),
    (5, DEFAULT, DEFAULT, 2000.0, 'say it aint so', 3),
    -- zach
    (6, DEFAULT, DEFAULT, 30.0, 'sussy', 4),
    (7, DEFAULT, DEFAULT, 300.0, 'dumpies', 4),
    (8, DEFAULT, DEFAULT, 3000.0, 'haHAH', 4),
    (9, DEFAULT, DEFAULT, 30000.0, 'strimmingpcpls', 4);

TRUNCATE claim_response;
INSERT INTO claim_response (response_id, resp_body, last_update, claim_id)
    VALUES
    (0, 'test body', DEFAULT, 0);

TRUNCATE claim_file;
INSERT INTO claim_file (c_file_id, file_path, claim_id)
    VALUES
    (0, 'test body', DEFAULT, 0);