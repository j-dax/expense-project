DROP TABLE IF EXISTS roles, employee, employee_login, claim, claim_response, claim_file CASCADE;

CREATE TABLE roles (
    role_id     SERIAL PRIMARY KEY,
    perms       varchar(3) NOT NULL,
    descr       varchar(280) NOT NULL
);

CREATE TABLE employee (
    employee_id     SERIAL PRIMARY KEY,
    api_key         VARCHAR,
    employee_name   varchar(45) NOT NULL,
    role_id         SERIAL NOT NULL
        CONSTRAINT fk_employee_role_id
        REFERENCES roles(role_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE employee_login (
    username    varchar(128),
    passhash    varchar(128),
    login_id    SERIAL PRIMARY KEY
        CONSTRAINT fk_employee_login_id
        REFERENCES employee(employee_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE claim (
    claim_id        SERIAL PRIMARY KEY,
    last_update     date DEFAULT NOW(),
    claim_status    varchar(8) DEFAULT 'PENDING',
    amount          decimal(15,2),
    claim_body      varchar(280),
    filed_by        SERIAL
        CONSTRAINT fk_claim_filed_by
        REFERENCES employee(employee_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE claim_response (
    response_id     SERIAL PRIMARY KEY,
    resp_body       varchar(280),
    last_update     date DEFAULT NOW(),
    claim_id        SERIAL
        CONSTRAINT fk_claimresp_claim_id
        REFERENCES claim(claim_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE claim_file (
    c_file_id   SERIAL PRIMARY KEY,
    file_path   varchar(260), --w10 default filepath length limit
    claim_id    SERIAL NOT NULL
        CONSTRAINT fk_claimfile_claim_id
        REFERENCES claim(claim_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE OR REPLACE FUNCTION trg_update_timestamp_on_update()
    RETURNS TRIGGER AS $$
    BEGIN
        NEW.last_update = NOW();
        RETURN NEW;
    END;
$$ language 'plpgsql';

CREATE TRIGGER tr_timestamp_update_claim
    BEFORE UPDATE ON claim
    FOR EACH ROW
    EXECUTE PROCEDURE trg_update_timestamp_on_update();

CREATE TRIGGER tr_timestamp_update_claim_resp
    BEFORE UPDATE ON claim_response
    FOR EACH ROW
    EXECUTE PROCEDURE trg_update_timestamp_on_update();
