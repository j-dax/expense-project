from src.config.flask_config import app
from src.controller.session_url import force_authenticate
from src.model.role import EmployeeRole
from src.model.claim import ClaimResponse
from src.service import claim_service, claim_resp_service
from src.config.logging_config import route_logger as logger

from flask import make_response, jsonify, request
from json import loads


@app.route("/api/claim/<int:claim_id>/response", methods=["POST"])
@force_authenticate
def empl_9(claim_id, employee, role):
    """POST a claim response for a particular employee
    Managers may view any claim/claim_response/claim_file
    Employees can only view files and responses on their own claims"""
    # differences in how roles are handled
    claim = claim_service.get_claim_by_claim_id(claim_id)
    if not claim or (isinstance(role, EmployeeRole) and claim.empl_id != employee.employee_id):
        logger.info(f"{request.method} {request.base_url} failed due to access perms or nonexistent file")
        return make_response(jsonify({}), 404)

    json = loads(request.get_data())
    # resp_id, resp_body, last_update, claim_id
    cr = ClaimResponse(-1, json.get("resp_body", ""), "", claim_id)
    cr = claim_resp_service.enter_claim_response(cr)

    logger.info(f"{request.method} {request.base_url} success {cr.__dict__}")
    return make_response(jsonify(cr.__dict__ if cr else {}), 200)


@app.route("/api/claim/<int:claim_id>/response")
@force_authenticate
def empl_8(claim_id, employee, role):
    """GET a claim response for a particular employee
    Managers may view any claim/claim_response/claim_file
    Employees can only view files and responses on their own claims"""
    # differences in how roles are handled
    claim = claim_service.get_claim_by_claim_id(claim_id)
    if not claim or (isinstance(role, EmployeeRole) and claim.empl_id != employee.employee_id):
        logger.info(f"{request.method} {request.base_url} failed")
        return make_response(jsonify({}), 404)
    cr = claim_resp_service.get_claim_response_by_claim_id(claim_id)
    logger.info(f"{request.method} {request.base_url} success {cr.__dict__}")
    return make_response(jsonify(cr.__dict__ if cr else {}), 200)
