from werkzeug.utils import redirect
from hashlib import sha512
from flask import request, make_response
from os import environ

from src.config.flask_config import app
from src.config.logging_config import route_logger as logger
from src.service import employee_service, login_service, role_service

logger.name = "session_url"


def force_authenticate(route):
    def decorated(*args, **kwargs):
        api_key = request.cookies.get("x-api-key", None)
        if not api_key:
            logger.info(f"{request.method} {request.base_url} not authenticated")
            return redirect(f'{app.config["FRONTEND_URL"]}/login')

        empl = employee_service.get_employee_by_api_key(api_key)
        role = role_service.get_role_by_role_id(empl.emp_role)

        return route(*args, **kwargs, employee=empl, role=role)

    decorated.__name__ = route.__name__
    return decorated


@app.route("/api")
def test():
    return make_response({"hello": "world"}, 200)


@app.route("/api/login")
def verify_cookie():
    resp = make_response({}, 400)

    employee_id = request.cookies.get("employee_id", None)
    api_key = request.cookies.get("x-api-key", None)

    if employee_id and api_key:
        # verify this employee has that api key
        empl = employee_service.get_employee_by_id(employee_id)
        if empl.api_key == api_key:
            resp = make_response({}, 200)

    logger.info(f"{request.method} {request.url} {resp}")
    return resp


@app.route("/api/login", methods=["POST"])
def setup_login():
    resp = make_response({}, 404)
    pwd = request.form.get("password", None)
    usr = request.form.get("username", None)

    if pwd and usr:

        credentials = login_service.get_login_by_username(usr)
        salt = f'{environ["FLASK_SECRET_SAUCE"]}{pwd}'
        passhash = sha512(salt.encode()).hexdigest()

        if credentials and passhash == credentials.passhash:
            empl = employee_service.get_employee_by_id(credentials.login_id)

            resp = make_response({}, 200)
            resp.set_cookie(
                "x-id", str(empl.employee_id).encode(), max_age=600, samesite="strict"
            )
            resp.set_cookie(
                "x-api-key", empl.api_key.encode(), max_age=600, samesite="strict"
            )
            resp.set_cookie(
                "x-role",
                str(role_service.get_role_by_role_id(empl.emp_role)).encode(),
                max_age=600,
                samesite="strict",
            )
            resp.set_cookie(
                "x-name", empl.employee_name.encode(), max_age=600, samesite="strict"
            )

    logger.info(f"{request.method} {request.url} {resp}")
    return resp


@app.route("/api/logout", methods=["GET"])
def logout():
    resp = redirect(f'{app.config["FRONTEND_URL"]}/login')
    resp.set_cookie("x-api-key", b"", max_age=0)
    resp.set_cookie("x-id", b"", max_age=0)
    resp.set_cookie("x-role", b"", max_age=0)
    resp.set_cookie("x-name", b"", max_age=0)
    return resp
