from src.config.flask_config import app
from src.controller.session_url import force_authenticate
from src.model.claim import Claim
from src.model.role import EmployeeRole
from src.service import claim_service
from src.config.logging_config import route_logger as logger
from flask import jsonify, make_response, request
from json import loads


@app.route("/api/claim/<int:claim_id>", methods=["POST"])
@force_authenticate
def empl_5(claim_id, employee, role):
    """View a claim for a particular employee
    Managers may view any claim
    Employees can only view their own claims"""
    # differences in how roles are handled
    claim = claim_service.get_claim_by_claim_id(claim_id)
    json = loads(request.get_data())
    if (isinstance(role, EmployeeRole) and claim and claim.empl_id != employee.employee_id):
        return make_response(jsonify({}), 404)
    elif all(["amount" not in json, "claim_body" not in json, "claim_status" not in json]):
        return make_response({}, 400)
    elif not claim:
        if any(["amount" not in json, "claim_body" not in json, "claim_status" not in json]):
            return make_response({}, 400)
        # claim_id, last_update, claim_status, amount, claim_body, empl_id):
        claim = Claim(-1, "", "PENDING", -1, "", employee.employee_id)

    claim.claim_status = json.get("claim_status", claim.claim_status)
    claim.amount = json.get("amount", claim.amount)
    claim.claim_body = json.get("claim_body", claim.claim_body)

    claim = claim_service.enter_claim(claim)
    logger.info(f"{request.method} {request.base_url} success {claim}")
    return make_response(jsonify(claim.__dict__ if claim else {}), 200)


@app.route("/api/claim/<int:claim_id>")
@force_authenticate
def empl_4(claim_id, employee, role):
    """View a claim for a particular employee
    Managers may view any claim
    Employees can only view their own claims"""
    # differences in how roles are handled
    claim = claim_service.get_claim_by_claim_id(claim_id)
    if not claim or (isinstance(role, EmployeeRole) and claim.empl_id != employee.employee_id):
        return make_response(jsonify({}), 404)
    logger.info(f"{request.method} {request.base_url} success {claim}")
    return make_response(jsonify(claim.__dict__ if claim else {}), 200)


@app.route("/api/claim")
@force_authenticate
def empl_3(employee, role):
    """GET - view all available for this user's role
    If authenticated and manager, show all claims
    If authenticated and employee matches employee_id, show all claims added by that empl
    If not authenticated, give no results"""
    claims = []
    claims = claim_service.get_claims_by_employee_id(employee.employee_id)
    logger.info(f"{request.method} {request.base_url} success, found {len(claims)} claims")
    return jsonify({"claims": [c.__dict__ for c in claims]})


@app.route("/api/claim", methods=["POST"])
@force_authenticate
def submit_claim(employee, role):
    """POST - submit a new claim
    must be authenticated"""
    description = request.form.get("description", "")
    amount = float(request.form.get("amount", -1))
    # claim_id, last_update, claim_status, amount, claim_body, empl_id
    if description and amount:
        claim = Claim(-1, "", "PENDING", amount, description, employee.employee_id)
        claim = claim_service.create_claim(claim)
        logger.info(f"{request.method} {request.base_url} success {claim}")
        return make_response(jsonify(claim.__dict__), 200)
    else:
        logger.info(f"{request.method} {request.base_url} failed")
        return make_response(f"Malformed form data:\ndescription={description}\namount={amount}", 400)
