from src.config.flask_config import app
from src.config.logging_config import route_logger as logger
from src.controller.session_url import force_authenticate
from src.model.role import ManagerRole
from src.service import claim_service
from flask import make_response, request


@app.route("/api/report")
@force_authenticate
def empl_7(employee, role):
    if isinstance(role, ManagerRole):
        claims = claim_service.get_all_claims()
        logger.info(f"{request.method} {request.base_url} success found {len(claims)} claims")
        return make_response({"claims": [c.__dict__ for c in claims]}, 200)
    logger.info(f"{request.method} {request.base_url} failed")
    return make_response("Not authorized", 400)


@app.route("/api/aggregate")
@force_authenticate
def get_aggregate(employee, role):
    if isinstance(role, ManagerRole):
        claims = claim_service.get_aggregated_report()
        logger.info(f"{request.method} {request.base_url} success found {len(claims)} claims")
        return make_response(claims, 200)
    logger.info(f"{request.method} {request.base_url} failed")
    return make_response("Not authorized", 400)
