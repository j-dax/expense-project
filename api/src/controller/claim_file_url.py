from src.config.flask_config import app
from src.controller.session_url import force_authenticate
from src.model.role import EmployeeRole
from src.model.claim import ClaimFile
from src.service import claim_service, claim_file_service
from src.config.logging_config import route_logger as logger


from os import path
from werkzeug.utils import secure_filename
from flask import (
    make_response,
    jsonify,
    request,
    send_file,
)


def allowed_file(filename):
    ALLOWED_EXTENSIONS = {"txt", "pdf", "png", "jpg", "jpeg", "gif"}
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/api/claim/<int:claim_id>/upload")
@force_authenticate
def empl_C(claim_id, employee, role):
    """GET a claim response for a particular employee
    Managers may view any claim/claim_response/claim_file
    Employees can only view files and responses on their own claims"""
    # differences in how roles are handled
    claim = claim_service.get_claim_by_claim_id(claim_id)
    if not claim or (
        isinstance(role, EmployeeRole) and claim.empl_id != employee.employee_id
    ):
        return make_response(jsonify({}), 404)
    cfs = claim_file_service.get_files_by_claim_id(claim_id)
    logger.info(f"{request.method} {request.base_url} found {len(cfs)} files")
    return make_response(
        jsonify({"files": [cf.__dict__ for cf in cfs if cf] if cfs else []}), 200
    )


@app.route("/api/claim/<int:claim_id>/upload", methods=["POST"])
@force_authenticate
def upload_file(claim_id, employee, role):
    file = request.files.get("file", None)
    named_as = request.form.get("named_as", "")

    if not file or file.filename == "" or named_as == "":
        logger.info(f"{request.method} {request.base_url} failed to upload with malformed name")
        return make_response({}, 400)

    if file and allowed_file(named_as):
        cf_id = int(request.form.get("cf_id", -1))
        mimetype = file.content_type

        filename = secure_filename(named_as)

        cf = ClaimFile(cf_id, filename, mimetype, claim_id)
        cf = claim_file_service.enter_file(cf)

        if cf:
            file.save(path.join(app.config["UPLOAD_FOLDER"], filename))
            logger.info(f"{request.method} {request.base_url} uploaded {cf}")
            return make_response(jsonify(cf.__dict__ if cf else {}), 200)
        else:
            logger.info(f"{request.method} {request.base_url} failed to upload")
            return make_response(
                jsonify(
                    {
                        "claim_id": claim_id,
                        "filename": named_as,
                        "mimetype": mimetype,
                        "cf_id": cf_id,
                    }
                ),
                400,
            )

    logger.info(f"{request.method} {request.base_url} failed to upload")
    return make_response({}, 400)


@app.route("/api/claim/<int:claim_id>/upload/<name>")
# @force_authenticate  #  This route has unexpected issues with verifying the connected user
# because the cookie is no longer present when they navigate directly on our route
def download_file(claim_id, name):
    cf = claim_file_service.get_claim_file_by_cid_name(claim_id, name)
    logger.info(f"{request.method} {request.base_url} served file {cf.__dict__}")
    return send_file(path.join(app.config["UPLOAD_FOLDER"], name), mimetype=cf.mimetype)
    # return send_from_directory(app.config["UPLOAD_FOLDER"], cf.file_path, mimetype=cf.mimetype)
