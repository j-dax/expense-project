from flask.helpers import make_response
from src.config.flask_config import app
from src.model.role import EmployeeRole, ManagerRole
from src.service import employee_service, role_service, claim_service
from src.controller.session_url import force_authenticate
from flask import redirect, jsonify
from flask_cors import cross_origin


@app.route("/api/employee", methods=["GET", "POST"])
def empl_0():
    """GET - list all employees
    POST - create new employee"""
    pass


@app.route("/api/employee/<int:employee_id>", methods=["PUT"])
def empl_1(employee_id):
    """PUT - create/update existing"""
    pass
