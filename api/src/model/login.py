class Login:
    def __init__(self, login_id, username, passhash):
        self.login_id = login_id
        self.username = username
        self.passhash = passhash

    def __eq__(self, other):
        return (
            self.login_id == other.login_id
            and self.username == other.username
            and self.passhash == other.passhash
        )
