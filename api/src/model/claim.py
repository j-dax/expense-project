from pathlib import Path


class ClaimFile:
    def __init__(self, c_file_id, file_path, mimetype, claim_id):
        self.c_file_id = c_file_id
        self.file_path = file_path
        self.mimetype = mimetype
        self.claim_id = claim_id


class ClaimResponse:
    def __init__(self, resp_id, resp_body, last_update, claim_id):
        self.resp_id = resp_id
        self.claim_id = claim_id
        self.last_update = last_update
        self.resp_body = resp_body


class Claim:
    def __init__(self, claim_id, last_update, claim_status, amount, claim_body, empl_id):
        self.claim_id = claim_id
        self.last_update = str(last_update)
        self.empl_id = empl_id
        self.amount = float(amount)
        self.claim_body = claim_body
        self.claim_status = claim_status
