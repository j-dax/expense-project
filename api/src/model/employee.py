class Employee:
    def __init__(self, api_key, emp_name, emp_role, emp_id=-1):
        self.api_key = api_key
        self.employee_name = emp_name
        self.emp_role = emp_role
        self.employee_id = emp_id

    def __str__(self):
        return f"""Employee {{
            \"employee_id\": {self.employee_id},
            \"name\": \"{self.employee_name}\",
            \"role\": \"{self.role}\"
            }}"""

    def __eq__(self, other):
        return (
            self.api_key == other.api_key
            and self.employee_name == other.employee_name
            and self.emp_role == other.emp_role
            and self.employee_id == other.employee_id
        )
