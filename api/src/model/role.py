class DefaultRole:
    def __str__(self):
        return ""

    def __repr__(self):
        return self.__class__.__name__


class EmployeeRole(DefaultRole):
    def __str__(self):
        return "EMP"


class ManagerRole(DefaultRole):
    def __str__(self):
        return "MGR"


class RoleFactory:
    role_translation: dict = {
        str(cls()): cls for cls in [EmployeeRole, ManagerRole]
    }

    @staticmethod
    def get_role(perm):
        if perm not in RoleFactory.role_translation:
            return DefaultRole()
        return RoleFactory.role_translation[perm]()
