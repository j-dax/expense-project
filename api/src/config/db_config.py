import psycopg2
from os import environ
from pathlib import Path


def load_dotenv(default_path=".env"):
    p = Path(default_path)
    with open(p) as dotenv:
        for line in dotenv:
            key_args = line.rstrip().split("=")
            environ[key_args[0]] = key_args[1]


def get_connection():
    """Get the database connection."""
    # load .env file with db info
    # return psycopg2.connect(
    #     host=environ["DATABASE_URI"],
    #     port=environ["PORT"],
    #     user=environ["DATABASE_USER"],
    #     password=environ["DATABASE_PASS"],
    #     database=environ["DATABASE"])
    return psycopg2.connect(
        host=environ["TEST_URI"],
        port=environ["PORT"],
        user=environ["TEST_USER"],
        password=environ["TEST_PASS"],
        database=environ["DATABASE"])


def add_test_data():
    lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("""TRUNCATE claim_file, claim_response, claim, employee_login, employee, roles CASCADE""")
            cur.executemany("""INSERT INTO roles (role_id, perms, descr) VALUES (%s, %s, %s)""", (
                            (0, 'MGR', 'Manager'),
                            (1, 'EMP', 'Employee')))
            cur.executemany("""INSERT INTO employee (employee_id, api_key, employee_name, role_id)
                            VALUES (%s, %s, %s, %s)""", (
                            (1, 'totallyuniqueapikey1', 'a', 0),
                            (2, 'totallyuniqueapikey2', 'c', 1),
                            (3, 'totallyuniqueapikey3', 'b', 1),
                            (4, 'totallyuniqueapikey4', 'd', 1)))
            cur.executemany("""INSERT INTO employee_login (username, passhash, login_id)
                            VALUES (%s, %s, %s)""", (
                            ('admin','c02e18b398b6c77095e14b1877fbf0697184fd88d7c7673a5a68e799435cac8c1f728cdc3e291cc15bec14db43d0df69d6b99a51f32b1c2917a326bc6cc399ff',1),
                            ('emp1','0cb877f97db4caf0b995f78dc8f92d5c92ba0b8638bb69d836071a8fe8ebe96013b755032629b712e0b4bb66aa7381b8ab761602b9563e782894c002b484de67',2),
                            ('emp2','3837ea3f82a94ea2422430c0647aa363b8be6f391dff56a7054804b38c14535cb4d45d102fadcc1fd91414b06a4ab764b33e555ccf68a9291a18c41404741e74',3),
                            ('emp3','6406df24695ca175c2b313579095a4ec2cb798e2d8d537112269a390489ef3794ff986c04eea86883f4d6f1dcad417a84528a4cc50046b8ed0fc44cad1a62143',4)))
            cur.executemany("""INSERT INTO claim (claim_id, last_update, claim_status, amount, claim_body, filed_by)
                            VALUES (%s,DEFAULT,DEFAULT,%s,%s,%s)""", (
                            (0, 1.0, 'Example description', 1),
                            (1, 10.0, f'Example description: {lorem_ipsum}', 2),
                            (2, 100.0, f'A: {lorem_ipsum}', 2),
                            (3, 20.0, f'B: {lorem_ipsum}', 3),
                            (4, 200.0, f'C: {lorem_ipsum}', 3),
                            (5, 2000.0, f'D: {lorem_ipsum}', 3),
                            (6, 30.0, f'E: {lorem_ipsum}', 4),
                            (7, 300.0, f'F: {lorem_ipsum}', 4),
                            (8, 3000.0, f'G: {lorem_ipsum}', 4),
                            (9, 30000.0, f'H: {lorem_ipsum}', 4)))
            # ensure claim_ids are at the correct value after truncating
            cur.execute("""SELECT setval(pg_get_serial_sequence('claim', 'claim_id'), (SELECT MAX(claim_id) FROM claim))""")
            cur.execute("""INSERT INTO claim_response (response_id, resp_body, last_update, claim_id)
                        VALUES (%s, %s, DEFAULT, %s)""",
                        (0, 'test body', 0))
        conn.commit()


def reset_database():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("""DROP TABLE IF EXISTS
                        roles,
                        employee,
                        employee_login,
                        claim,
                        claim_response,
                        claim_file CASCADE""")
            cur.execute("""CREATE TABLE roles (
                        role_id     SERIAL PRIMARY KEY,
                        perms       varchar(3) NOT NULL,
                        descr       varchar(280) NOT NULL)""")
            cur.execute("""CREATE TABLE employee (
                        employee_id     SERIAL PRIMARY KEY,
                        api_key         VARCHAR,
                        employee_name   varchar(45) NOT NULL,
                        role_id         SERIAL NOT NULL
                            CONSTRAINT fk_employee_role_id
                            REFERENCES roles(role_id)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE)""")
            cur.execute("""CREATE TABLE employee_login (
                username    varchar(128),
                passhash    varchar(128),
                login_id    SERIAL PRIMARY KEY
                    CONSTRAINT fk_employee_login_id
                    REFERENCES employee(employee_id)
                    ON DELETE CASCADE
                    ON UPDATE CASCADE)""")
            cur.execute("""CREATE TABLE claim (
                        claim_id        SERIAL PRIMARY KEY,
                        last_update     date DEFAULT NOW(),
                        claim_status    varchar(8) DEFAULT 'PENDING',
                        amount          decimal(15,2),
                        claim_body      varchar(280),
                        filed_by        SERIAL
                            CONSTRAINT fk_claim_filed_by
                            REFERENCES employee(employee_id)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE)""")
            cur.execute("""CREATE TABLE claim_response (
                        response_id     SERIAL PRIMARY KEY,
                        resp_body       varchar(280),
                        last_update     date DEFAULT NOW(),
                        claim_id        SERIAL
                            CONSTRAINT fk_claimresp_claim_id
                            REFERENCES claim(claim_id)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE)""")
            cur.execute("""CREATE TABLE claim_file (
                        c_file_id   SERIAL PRIMARY KEY,
                        file_path   varchar(260), --w10 default filepath length limit
                        mimetype    varchar(40) NOT NULL,
                        claim_id    SERIAL NOT NULL
                            CONSTRAINT fk_claimfile_claim_id
                            REFERENCES claim(claim_id)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE)""")
            cur.execute("""CREATE OR REPLACE FUNCTION trg_update_timestamp_on_update()
                            RETURNS TRIGGER AS $$
                            BEGIN
                                NEW.last_update = NOW();
                                RETURN NEW;
                            END;
                        $$ language 'plpgsql';""")
            cur.execute("""CREATE TRIGGER tr_timestamp_update_claim
                        BEFORE UPDATE ON claim
                        FOR EACH ROW
                        EXECUTE PROCEDURE trg_update_timestamp_on_update()""")
            cur.execute("""CREATE TRIGGER tr_timestamp_update_claim_resp
                        BEFORE UPDATE ON claim_response
                        FOR EACH ROW
                        EXECUTE PROCEDURE trg_update_timestamp_on_update()""")
        conn.commit()
