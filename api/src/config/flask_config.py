from flask import Flask
from pathlib import Path
import flask_cors


app = Flask(__name__, static_folder=Path("../../static"))
app.debug = True
app.config["FRONTEND_URL"] = "http://127.0.0.1:3000"
app.config["UPLOAD_FOLDER"] = Path(app.static_folder, "upload")
flask_cors.CORS().init_app(app)


@app.after_request
def set_headers(resp):
    resp.headers['Cache-Control'] = 'no-store'
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp
