import logging
from pathlib import Path
from os import mkdir

__DEFAULT_LOGGING_LEVEL__ = logging.DEBUG


def get_logger(filename: str, directory: Path = Path("log"), level=__DEFAULT_LOGGING_LEVEL__):
    """Return a preconfigured logger.

        filename: the name of the file

        directory: the pathlike directory
    """
    try:
        mkdir(directory)
    except FileExistsError:
        pass

    handler = logging.FileHandler(directory.joinpath(filename))
    formatter = logging.Formatter(
        fmt='%(asctime)s %(name)s :: %(levelname)-8s :: %(message)s')
    handler.setFormatter(formatter)

    logger = logging.getLogger(filename)
    logger.addHandler(handler)
    logger.setLevel(level)

    # ensure filename is not ""
    filename = filename if filename else "default.log"

    return logger


test_logger = get_logger("test.log", level=logging.INFO)
route_logger = get_logger("route.log", level=logging.INFO)
