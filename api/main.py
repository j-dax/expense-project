from src.config.flask_config import app
from src.controller import (
    employee_url,
    session_url,
    claim_url,
    manager_url,
    claim_response_url,
    claim_file_url
)
from src.config.db_config import reset_database, add_test_data
from src.config.db_config import load_dotenv


if __name__ == "__main__":
    load_dotenv("api/.env")
    reset_database()
    add_test_data()
    app.run(debug=True)
