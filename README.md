# Expense Software

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can log in and submit requests for reimbursements and view their past tickets and pending requests. Managers can log in and view all reimbursement requests and past history for all employees in the company. Managers are authorized to approve and deny requests for expense reimbursements.

## Technologies Used

### Backend Technologies

* Python
* Flask
* PostgreSQL with Psycopg2
* Selenium
* Gherkin

### Frontend Technologies

* Javascript
* React
* SCSS
* HTML

## Features

* Expense Software supports and manages logins under a RESTful API
* Employees and managers can log in to submit requests for reimbursements
* Employees and managers may view their past tickets and pending requests
* Managers can view all reimbursement requests and past history for all employees in the company
* Managers are authorized to approve and deny requests for expense reimbursements.

## Getting Started

The following was setup on postgres, python3 and npm to get started. Find them here:

> [postgres odbc](https://www.postgresql.org/ftp/odbc/versions/)

> [postgres](https://www.postgresql.org/download/)

> [python](https://www.python.org/downloads/)

> [npm](https://nodejs.org/en/download/)

```
git clone https://gitlab.com/j-dax/expense-project
cd expense-project
```

Setup your python virtual environment, I prefer pipenv
optionally, use a virtual environment for the separate selenium (./test) and flask (./api) folders

```
pipenv shell
pip install -r api/requirements.txt -r test/requirements.txt
```

Setup the React project in ./web

```
cd web
npm install -g yarn
yarn install
```

## Usage

Pop a couple shells to start up the backend and frontend.

```
python api/main.py
yarn start
```

The current project configures the database and boots the flask server on success,
and the frontend proxies results from the api. 

(After starting the servers, the site will be available here)[http://localhost:3000]

Navigate to the login page and login with "admin" "admin" to view the rest of the site with example data.

<img src="./login.png">

## License

This project uses the following license: [MIT](https://gitlab.com/j-dax/expense-project/-/blob/main/LICENSE).

