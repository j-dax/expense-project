import * as React from 'react';
import './ModalLayout.scss'

function hasClassNameValue(element, idValue) {
    // the element may not have an id attribute, so treat that as undefined
    return element.attributes.class?.value === idValue;
}

function elementHasAncestorWithClassName(element, idValue) {
    let parent = element;
    while (parent.tagName !== "BODY" && !hasClassNameValue(parent, idValue)) {
        parent = parent.parentElement;
    }
    return parent.tagName !== "BODY" && hasClassNameValue(parent, idValue);
}


const ModalLayout = ({children, modalTitle, buttonText="Open"}) => {
    let ref = React.useRef(undefined);

    function openModal(e) {
        let prevSibling = e.target.previousElementSibling;
        prevSibling.style.display = "block";
        ref.current = prevSibling;
    }

    function closeModal(e) {
        if (!elementHasAncestorWithClassName(e.target, "modal-constrictor") ||
            hasClassNameValue(e.target, "close")) {
            ref.current.style.display = "none";
            ref.current = undefined;
        }
    }

    return (<>
        <div className="modal-container" onClick={closeModal}>
            <div className="modal-constrictor">
                <span>{modalTitle}</span>
                <span className="close">&times;</span>
                <div className="modal-content">
                    <div>{children}</div>
                </div>
            </div>
        </div>
        <button id="modal-button" onClick={openModal}>{buttonText}</button>
    </>)
}

export default ModalLayout;