import * as React from 'react';
// React prevents direct access to the head, entry is html.body.div#app
import {Helmet, HelmetProvider} from "react-helmet-async";

import "./BootstrapLayout.scss";
import ExpenseNav from '../../component/ExpenseNav/ExpenseNav';

const BootstrapLayout = ({title="default title", children}) => {
    let [ managedEmployeeState, setEmployeeState ] = React.useState(false);
    let [ managedEmployeeId, setEmployeeId ] = React.useState(-1);
    // inject employee state in a small package
    let employee_state = {
        state: {
            employeeId: ()=>managedEmployeeId,
            employeeState: ()=>managedEmployeeState
        },
        setter: {
            employeeId: setEmployeeId,
            employeeState: setEmployeeState
        }
    }

    let unmanagedChildren = React.Children.map(children, child =>{
        if (React.isValidElement(child))
            return React.cloneElement(child, { employee_state });
        return child;
    });

    return (<HelmetProvider>
            <Helmet>
                <title>{title}</title>
            </Helmet>
            <ExpenseNav />
            <main id="expense-content">
                {unmanagedChildren}
            </main>
        </HelmetProvider>)
};

export default BootstrapLayout;