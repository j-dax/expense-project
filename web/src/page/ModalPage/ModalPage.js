import * as React from 'react';
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';
import ModalLayout from '../../layout/ModalLayout/ModalLayout';

const ModalPage = () => {
    return (<BootstrapLayout>
        <ModalLayout modalTitle="Mhm">
            <div>well done, sir</div>
        </ModalLayout>
    </BootstrapLayout>)
}

export default ModalPage;