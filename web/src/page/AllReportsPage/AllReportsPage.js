import * as React from 'react';
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';
import AllReportsTable from '../../component/AllReportsTable/AllReportsTable';


const AllReportsPage = () => {
    return (<BootstrapLayout>
        <AllReportsTable />
    </BootstrapLayout>)
}

export default AllReportsPage;