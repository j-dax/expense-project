import * as React from 'react';
import LoginForm from '../../component/LoginForm/LoginForm';
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';

const LoginPage = () => {
    return (
        <BootstrapLayout>
            <LoginForm />
        </BootstrapLayout>
    )
}

export default LoginPage;