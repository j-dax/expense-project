import * as React from 'react';
import { useParams } from 'react-router-dom';
import ReportView from '../../component/ReportView/ReportView';
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';

const ReportPage = () => {
    let { claimId } = useParams();
    return (<BootstrapLayout>
        <ReportView claimId={claimId} />
    </BootstrapLayout>)
}

export default ReportPage;