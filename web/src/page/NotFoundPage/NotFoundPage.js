import * as React from 'react'
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout'

const NotFoundPage = () => {
    return (
        <BootstrapLayout>
            <div>The page you are looking for is not here.</div>
        </BootstrapLayout>
    )
}

export default NotFoundPage;