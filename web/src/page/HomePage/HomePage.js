import * as React from 'react';
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';

const HomePage = () => {
    React.useEffect(()=>{
        fetch("/api")
        .then(resp=>resp.json())
        .then(json=>{
            for (let ele in json){
                document.getElementById("example").innerText += ele;
                document.getElementById("example").innerText += json[ele];
            }
        })
    });
    return (
        <BootstrapLayout>
            <div id="example"></div>
        </BootstrapLayout>
    )
}

export default HomePage;