import * as React from 'react';
import { useParams } from 'react-router-dom'
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';
import ModalLayout from '../../layout/ModalLayout/ModalLayout';
import ClaimView from '../../component/ClaimView/ClaimView'
import UploadForm from '../../component/UploadForm/UploadForm'

const ClaimPage = () => {
    let { claimId } = useParams();
    return (<>
        <BootstrapLayout>
            <ClaimView claimId={claimId} />
            
            <ModalLayout modalTitle="Upload a file" buttonText="Upload" >
                <UploadForm claimId={claimId} />
            </ModalLayout>

        </BootstrapLayout>
    </>)
};

export default ClaimPage;