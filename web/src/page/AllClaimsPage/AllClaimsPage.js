import * as React from 'react';
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';
import AllClaimsTable from '../../component/AllClaimsTable/AllClaimsTable';

const AllClaimsPage = () => {
    return (
        <BootstrapLayout>
            <AllClaimsTable />
        </BootstrapLayout>
    )
}

export default AllClaimsPage;