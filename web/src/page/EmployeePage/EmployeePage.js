import * as React from 'react';
import { useParams } from 'react-router-dom'
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';

const ClaimPage = () => {
    let { employeeId } = useParams();
    return (<>
        <BootstrapLayout>
            <div>Employee BB {employeeId}</div>
        </BootstrapLayout>
    </>);
}

export default ClaimPage;