import * as React from 'react';
import UploadForm from '../../component/UploadForm/UploadForm';
import BootstrapLayout from '../../layout/BootstrapLayout/BootstrapLayout';
import ModalLayout from '../../layout/ModalLayout/ModalLayout';

const UploadPage = () => {
    return (<BootstrapLayout>
        <ModalLayout modalTitle="Upload a file">
            <UploadForm />
        </ModalLayout>
    </BootstrapLayout>)
}

export default UploadPage;