import * as React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import NotFoundPage from '../page/NotFoundPage/NotFoundPage';

import LoginPage from '../page/LoginPage/LoginPage';
import ClaimPage from '../page/ClaimPage/ClaimPage';
import ReportPage from '../page/ReportPage/ReportPage';
import AllClaimsPage from '../page/AllClaimsPage/AllClaimsPage';
import AllReportsPage from '../page/AllReportsPage/AllReportsPage';
import HomePage from '../page/HomePage/HomePage';

const ExpenseRouter = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/report" component={AllReportsPage} />
                <Route exact path="/report/:claimId" component={ReportPage} />
                <Route exact path="/claim" component={AllClaimsPage} />
                <Route exact path="/claim/:claimId" component={ClaimPage} />
                <Route to="/404" component={NotFoundPage} />
                <Redirect to="/404" />
            </Switch>
        </BrowserRouter>
    )
}

export default ExpenseRouter;