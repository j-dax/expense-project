import * as React from 'react';
import Leaderboard from '../Leaderboard/Leaderboard';

let navigateTo = (url)=>{
    window.open(url,'_blank');
    window.open(url);
};

const AllReportsTable = () => {
    let [claims, setClaims] = React.useState([]);

    React.useEffect(()=>{
        fetch("/api/report")
        .then(resp=>resp.json())
        .then(json=>setClaims(json["claims"]))
    }, []);

    return (<div>
        <h1>ULTIMATE CLAIMS REPORT</h1>

        <div className="flex-report">

            <table>
                <thead>
                    <tr>
                        <th>Claim Id</th>
                        <th>Last Updated</th>
                        <th>Claim Body</th>
                        <th>Amount ($)</th>
                        <th>Submitted by</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {claims.map((claim, i)=><tr key={i} onClick={()=>navigateTo(`/report/${claim.claim_id}`)}>
                        <td>{claim.claim_id}</td>
                        <td>{claim.last_update}</td>
                        <td>{claim.claim_body}</td>
                        <td>{claim.amount}</td>
                        <td>{claim.empl_id}</td>
                        <td>{claim.claim_status}</td>
                    </tr>)}
                </tbody>
            </table>
            
            <Leaderboard />

        </div>
    </div>)
}

export default AllReportsTable;