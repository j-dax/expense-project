import * as React from 'react';
import './ClaimView.scss'

const downloadFile = (claimId, filePath) => {
    let url = `http://localhost:5000/api/claim/${claimId}/upload/${filePath}`;
    
    fetch(url)
    .then(resp=>resp.blob())
    .then(blob => {
        url = window.URL.createObjectURL(blob);
        let link = document.createElement("a");
        link.download = filePath;
        link.href = url;
        link.click();
    });
}

const ClaimView = ({claimId}) => {
    let [claim, setClaim] = React.useState({})
    let [claimResponse, setClaimResponse] = React.useState({})
    let [claimFileList, setClaimFileList] = React.useState([])

    React.useEffect(()=>{
        fetch(`/api/claim/${claimId}`)
        .then(resp=>resp.json())
        .then(json=>setClaim(json))
        .catch(err=>console.log(err));
    }, [claimId])
    React.useEffect(()=>{
        fetch(`/api/claim/${claimId}/response`)
        .then(resp=>resp.json())
        .then(json=>setClaimResponse(json))
        .catch(err=>console.error(err));
    }, [claimId])
    React.useEffect(()=>{
        fetch(`/api/claim/${claimId}/upload`)
        .then(resp=>resp.json())
        .then(json=>setClaimFileList(json["files"]))
        .catch(err=>console.error(err));
    }, [claimId])

    return (<>
        <h1>Claim {claimId}</h1>
        <div className="flex">
            <div id="claims-container" className="red">
            {Object.keys(claim).length > 0 && <>
                <p>Filed by: {claim.empl_id}</p>
                <p>Last Updated: {claim.last_update}</p>
                <p>Description: {claim.claim_body}</p>
                <p>Amount: {claim.amount}</p>
                <p>Status: {claim.claim_status}</p>
            </>}
            </div>

            <div id="response-container" className="blue">
            {Object.keys(claimResponse).length > 0 && <>
                <p>Sanity Check Id: {claimResponse.resp_id}</p>
                <p>Desciption: {claimResponse.resp_body}</p>
                <p>Date Response: {claimResponse.last_update}</p>
            </>}
            </div>

            <div id="file-container" className="green">
                {claimFileList?.length > 0 && <table>
                    <thead>
                        <tr>
                            <th>Sanity Check Id</th>
                            <th>On Claim Id</th>
                            <th>Filename</th>
                            <th>Mimetype</th>
                        </tr>
                    </thead>
                    <tbody>
                    {claimFileList.map((claimFile, cid) =>
                        <tr key={cid} onClick={()=>downloadFile(claimFile.claim_id, claimFile.file_path)}>
                            <td>{claimFile.c_file_id}</td>
                            <td>{claimFile.claim_id}</td>
                            <td>{claimFile.file_path}</td>
                            <td>{claimFile.mimetype}</td>
                        </tr>)}
                    </tbody>
                </table>}
            </div>
        </div> 
    </>)
}

export default ClaimView;