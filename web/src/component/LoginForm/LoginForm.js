import * as React from 'react';
import './LoginForm.scss';


const parseCookie = () => {
    let cookieJson = {};
    for (let ele of document.cookie.split("; ")) {
        let arr = ele.split("=");
        cookieJson[arr[0]] = arr[1];
    }
    return cookieJson;
}


const submitForm = (e) => {
    e.preventDefault();

    let form = document.getElementById("login-form");
    let fd = new FormData(form);

    fetch('/api/login', {
        method: 'POST',
        body: fd
    }).then(_=>{
        let json = parseCookie();
        if ("x-id" in json) { // on successful login
            window.location.href = `/claim`;
        } else {
            document.getElementById("error-text").innerText = "Invalid user credentials.";
        }
    });
}

const LoginForm = () => {
    return (
        <form id='login-form'>  
            <label>Username</label>
            <input name="username" type="text" placeholder="Username" />
            <label>Password</label>
            <input name="password" type="password" placeholder="Password" />
            <p id="error-text"></p>
            <input type="submit" value="Submit" onClick={submitForm} />
        </form>
    );
}

export default LoginForm;