import * as React from 'react'

const getFormData = (e) => {
    let form = document.getElementById("claim-form");
    let fd = new FormData(form);

    return fd
}

function submitAndRefresh(e) {
    e.preventDefault();

    let opts = {
        method: 'POST',
        body: getFormData()
    }

    fetch('/api/claim', opts)
    .then(resp=>{
        if (resp.status === 200) {
            window.location.reload(false)
        }
    })

}

const ClaimForm = () => {
    return (
        <form id="claim-form">
            <input name='description' type='text' placeholder='Description' required />
            <input name='amount' type='number' placeholder='0.00' step='0.01' min='0.00' max='100000.00' required />
            <input type='submit' value='Submit' onClick={submitAndRefresh} />
        </form>
    )
}

export default ClaimForm;