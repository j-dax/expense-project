import * as React from 'react'
import './Leaderboard.scss'

const Leaderboard = () => {
    let [aggregatedData, setData] = React.useState({"data": []});

    React.useEffect(()=>{
        fetch("/api/aggregate")
        .then(resp=>resp.json())
        .then(json=>{
            if ("data" in json) {
                setData(json)
            }
        });
    }, []);

    return (<>
       {aggregatedData.data.length > 0 && <table id="aggregateTable">
            <thead>
                <tr>
                    <th>Employee Id</th>
                    <th>Claims Submitted</th>
                    <th>Sum of Claims</th>
                </tr>
            </thead>
            <tbody id="aggregateTableBody">
                {aggregatedData.data.map((dat, i)=><tr key={i}>
                    <td>{dat["employee_id"]}</td>
                    <td>{dat["claims_count"]}</td>
                    <td>{dat["claims_sum"]}</td>
                </tr>)}
       </tbody></table>}
    </>);
}

export default Leaderboard;