import * as React from 'react';
import { Link } from "react-router-dom";
import "./ExpenseNav.scss"


const parseCookie = () => {
    let cookieJson = {}
    for (let ele of document.cookie.split("; ")) {
        let arr = ele.split("=");
        cookieJson[arr[0]] = arr[1];
    }
    if ("x-role" in cookieJson && cookieJson["x-role"] === "MGR") {
        cookieJson["x-role"] = true;
    } else { cookieJson["x-role"] = false; }

    if (!("x-id" in cookieJson)) {
        cookieJson["x-id"] = -1;
    } else {
        cookieJson["x-id"] = Number(cookieJson["x-id"]);
    }

    if (!("x-name" in cookieJson)) {
        cookieJson["x-name"] = undefined;
    }

    return cookieJson;
}
function deleteCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    sessionStorage.removeItem(name);
}


const ExpenseNav = () => {
    let cookieJson = parseCookie();

    let employeeId = React.useRef(cookieJson["x-id"]);
    let isManager = React.useRef(cookieJson["x-role"]);
    let name = React.useRef(cookieJson["x-name"]);

    const resetValues = () => {
        employeeId.current = -1;
        isManager.current = false;
        name.current = undefined;

        deleteCookie("x-id");
        deleteCookie("x-role");
        deleteCookie("x-name");
        deleteCookie("x-api-key");

        console.log("called resetValues")
        console.log(document.cookie)
    }

    return (<>
        <nav>
            <div id="nav-left">
                
                <Link to="/">
                    <button>Home</button>
                </Link>


                {isManager.current && <>
                    <div className="vertical-bar"></div>
                    <Link to={`/report`}>
                        <button>Reports</button>
                    </Link>
                </>}

                {employeeId.current >= 0 && <>
                    <div className="vertical-bar"></div>
                    <Link to="/claim">
                        <button>Claims</button>
                    </Link>
                </>}
            </div>

            <div id="login-div">
                {employeeId.current >= 0 && <>
                    <span>
                        {`Welcome, ${name.current}`}
                    </span>

                    <div className="vertical-bar"></div>

                    <Link to="/login" onClick={resetValues}>
                        <button>Logout</button>
                    </Link>
                </>}
                {employeeId.current === -1 &&
                    <Link to="/login">
                        <button>Login</button>
                    </Link>
                }
            </div>
        </nav>
    </>);
}

export default ExpenseNav;