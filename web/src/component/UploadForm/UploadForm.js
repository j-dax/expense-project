import * as React from 'react';

const getFormData = () => {
    let form = document.getElementById("upload-form");
    let fd = new FormData(form);

    if (!fd.get("file").name)
        return; // submitting no file, no-op

    if (!fd.get("named_as")) // submitting file with empty name
        fd.set("named_as", fd.get("file").name)

    // assert that the file has an extension
    // assert that the file's extension is present in the rename
    let ext = fd.get("file").name.split(".")
    ext = ext[ext.length - 1]
    // if the name contains `ext`, the split will have multiple results
    if (fd.get("named_as").split(ext).length === 1)
        fd.set("named_as", fd.get("named_as") + "." + ext)

    return fd
}

const UploadFile = ({claimId}) => {
    const doFetch = (e, url) => {
        e.preventDefault();
        let fd = getFormData()
        fetch(url, {
            method: 'POST',
            body: fd
        }).then(resp=>{
            if (resp.status === 200)
                window.location.reload(false);
        })
    }

    return (<>
        <h1>Upload new File</h1>
        <form id="upload-form" encType='multipart/form-data'>
            <input name='file' type='file' />
            <input name='named_as' type='text' placeholder="Rename file..." />
            <input name='cf_id' type='number' value={-1} hidden readOnly />
            <input type='submit' value='Upload' onClick={(e)=>doFetch(e, `/api/claim/${claimId}/upload`)} />
        </form>
    </>)
}

export default UploadFile;