import * as React from 'react';
import { Redirect } from 'react-router-dom';
import ModalLayout from '../../layout/ModalLayout/ModalLayout'
import ClaimForm from '../../component/ClaimForm/ClaimForm'
import "./AllClaimsTable.scss"

const parseCookie = () => {
    let cookieJson = {}
    for (let ele of document.cookie.split("; ")) {
        let arr = ele.split("=");
        cookieJson[arr[0]] = arr[1];
    }
    return cookieJson;
}

const navigateTo = (url) => {
    window.location.href = url; // same page
    // window.open(url,'_blank'); // new page
};

const AllClaimsTable = () => {

    let cookieJson = parseCookie();
    if (!("x-id" in cookieJson)) {
        cookieJson["x-id"] = -1;
    } else {
        cookieJson["x-id"] = Number(cookieJson["x-id"]);
    }
    
    let employeeId = React.useRef(cookieJson["x-id"]);
    let [claims, setClaims] = React.useState([]);
    React.useEffect(()=>{
        fetch(`/api/claim`)
        .then(resp=>{
            if (resp.ok)
                return resp.json()
        }).then(json=>{
            if ("claims" in json) // don't cause a rerender if this fetch failed
                setClaims(json["claims"])
        }).catch(err=>console.error(err))
    }, [])

    return (<div>
        {employeeId.current < 0 &&
        <Redirect to="/login" />}

        <ModalLayout modalTitle="Submit a claim" buttonText="New Claim">
            <ClaimForm />
        </ModalLayout>

        <fieldset>
            <legend>Employee claims for {employeeId.current}</legend>
            <table>
                <thead>
                    <tr>
                        <th>Claim Id</th>
                        <th>Last Updated</th>
                        <th>Claim Body</th>
                        <th>Amount ($)</th>
                        <th>Submitted by</th>
                        <th>Status</th>
                    </tr>
                </thead>
            
                <tbody id="claim-table-body">
                    {claims.map((claim, i)=>
                    <tr key={i} onClick={()=>navigateTo(`/claim/${claim.claim_id}`)}>
                        <td>{claim.claim_id}</td>
                        <td>{claim.last_update}</td>
                        <td>{claim.claim_body}</td>
                        <td>{claim.amount}</td>
                        <td>{claim.empl_id}</td>
                        <td>{claim.claim_status}</td>
                    </tr>)}
                </tbody>
            </table>
        </fieldset>

    </div>)
}

export default AllClaimsTable;