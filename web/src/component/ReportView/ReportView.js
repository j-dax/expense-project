import * as React from 'react';
import { Redirect } from 'react-router-dom'
import ClaimView from '../ClaimView/ClaimView';

const postDecision = (claimId, decision, reason) => {
    let opts = {
        method: 'POST',
        body: {}
    }
    opts.body = JSON.stringify({
        "claim_status": decision,
        "amount": undefined,
        "claim_body": undefined
    })

    fetch(`/api/claim/${claimId}`, opts)
    .then(_=> // refresh this page
        window.location.href = `/report/${claimId}`)
    .catch(err=>{
        console.error(err);
        document.getElementById("error-msg").innerText = err.message;
    });

    opts.body = JSON.stringify({
        "resp_body": reason
    });
    fetch(`/api/claim/${claimId}/response`, opts)
    .then(resp=>resp.json())
    .then(_=> // refresh this page
        window.location.href = `/report/${claimId}`)
    .catch(err=>{
        console.error(err);
        document.getElementById("error-msg").innerText = err.message;
    });
}

const ReportView = ({claimId}) => {
    let [input, setInput] = React.useState('');
    const responseOptions = ["Approve", "Reject", "Pending"];
    return (<div>
        {<Redirect to={`/report/${claimId}`} />}
        <ClaimView claimId={claimId} />
        
        <p id="error-msg"></p>
        <div className="btn-group">
            <input id="response-input" value={input} onInput={e => setInput(e.target.value)}/>
            <br/>
            {responseOptions.map((optString, i) => 
                <button key={i} onClick={()=>postDecision(claimId, optString.toUpperCase(), input)}>{optString}</button>
            )}
        </div>
        
    </div>);
}

export default ReportView;