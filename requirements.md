# Requirements

## General Requirements
| Req ID | Description |
| :---: | :---: |
| REST | Excluding login, backend must be REST-compliant |
| RDS | The project has a persistent database and must use AWS RDS |
| TEST | DAO and Service must be tested, using mock testing where applicable |
| LOG | Logging is implemented throughout the application |
| ACPT | Application must be modeled using user stories and acceptance testing |
| E2E | E2E tests using gherkin and selnium for all user stories. |
| ROLE | Users of the system must be assigned particular roles. |


## Employee Features
| Req ID | Description |
| :---: | :---: |
| AUTH | Employees must log in to see their past and pending reimbursements |
| API-EMP-0 | GET /claim must show all available claims for the logged in user. |
| API-EMP-1 | GET /claim/:claim_id must show one claim if it exists, else 404 |
| API-EMP-1 | PUT/POST /claim/:claim_id must add a claim. PUT will overwrite the claim with `claim_id`, POST will create a new claim. |
| API-EMP-2 | POST /claim/new must be able to submit a reimbursement with an amount and a reason |


## Manager Features
| Req ID | Description |
| :---: | :---: |
| VIEW | Manager must be able to view all reimbursements past and pending |
| OKAY | A Manager must be able to appove or deny any reimbursement |
| STATS | Managers have a statistics page where they can view various reports. "That includes information like what employee spends the most money, mean expenditure cost etc..." |

### Key Notes
- you do not have to allow for the creation of employees or managers.
    - You can have these already in the database.
- You do not need to have implement security for application. You can assume that a later security team is responsible for making the applicaiton secure.
    - API routes do not need to be protected
    - Passwords do not have to be encrypted
